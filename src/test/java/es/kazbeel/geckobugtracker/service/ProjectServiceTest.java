package es.kazbeel.geckobugtracker.service;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import es.kazbeel.geckobugtracker.builders.ComponentBuilder;
import es.kazbeel.geckobugtracker.builders.ProjectBuilder;
import es.kazbeel.geckobugtracker.builders.SectionBuilder;
import es.kazbeel.geckobugtracker.model.Component;
import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.model.ProjectVersion;
import es.kazbeel.geckobugtracker.model.Section;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.repository.ComponentDao;
import es.kazbeel.geckobugtracker.repository.ProjectDao;
import es.kazbeel.geckobugtracker.repository.ProjectVersionDao;
import es.kazbeel.geckobugtracker.repository.SectionDao;


@RunWith(MockitoJUnitRunner.class)
public class ProjectServiceTest {
    
    @Mock
    private SectionDao sectionDao;
    
    @Mock
    private ProjectDao projectDao;
    
    @Mock
    private ProjectVersionDao projectVersionDao;
    
    @Mock
    private ComponentDao componentDao;
    
    @InjectMocks
    private ProjectService projectService = new ProjectServiceImpl(sectionDao,
                                                                   projectDao,
                                                                   projectVersionDao,
                                                                   componentDao);
    
    
    @Test
    public void testGetAllSections () {
    
        projectService.getAllSections();
        
        verify(sectionDao).findAll();
    }
    
    @Test
    public void testGetSectionById () {
    
        projectService.getSectionById(anyLong());
        
        verify(sectionDao).findById(anyLong());
    }
    
    @Test
    public void testGetSectionByIdWithProjects_allOK () {
    
        Set<Project> projects = new HashSet<Project>(1);
        projects.add(ProjectBuilder.project().withId(1L).withName("project_name").build());
        
        Set<Project> spyProjects = spy(projects);
        
        Section section = SectionBuilder.section()
                                        .withId(1L)
                                        .withName("section_name")
                                        .withProjects(spyProjects)
                                        .build();
        
        when(projectService.getSectionByIdWithProjects(anyLong())).thenReturn(section);
        
        projectService.getSectionByIdWithProjects(anyLong());
        
        verify(sectionDao).findById(anyLong());
        verify(spyProjects).size();
    }
    
    @Test
    public void testGetSectionByIdWithProjects_sectionNotFound () {
    
        Set<Project> spyProjects = spy(new HashSet<Project>());
        
        when(projectService.getSectionByIdWithProjects(anyLong())).thenReturn(null);
        
        projectService.getSectionByIdWithProjects(anyLong());
        
        verify(sectionDao).findById(anyLong());
        verify(spyProjects, never()).size();
    }
    
    @Test
    public void testGetSectionByName () {
    
        projectService.getSectionByName("name");
        
        verify(sectionDao).findByName("name");
    }
    
    @Test
    public void testSetHigherOrderToSection () {
    
        Section mockSection = mock(Section.class);
        
        projectService.setHigherOrderToSection(mockSection);
        
        verify(mockSection).getId();
        verify(sectionDao).decreaseSortValue(anyLong());
    }
    
    @Test
    public void testSetLowerOrderToSection () {
    
        Section mockSection = mock(Section.class);
        
        projectService.setLowerOrderToSection(mockSection);
        
        verify(mockSection).getId();
        verify(sectionDao).increaseSortValue(anyLong());
    }
    
    @Test
    public void testSaveSection () {
    
        projectService.saveSection(any(Section.class));
        
        verify(sectionDao).saveOrUpdate(any(Section.class));
    }
    
    @Test
    public void testDeleteSection () {
    
        projectService.deleteSection(any(Section.class));
        
        verify(sectionDao).delete(any(Section.class));
    }
    
    @Test
    public void testGetProjectById () {
    
        projectService.getProjectById(anyLong());
        
        verify(projectDao).findById(anyLong());
    }
    
    @Test
    public void testGetProjectByIdWithComponentsAndVersions () {
    
        Project mockProject = mock(Project.class);
        
        when(projectDao.findById(anyLong())).thenReturn(mockProject);
        
        projectService.getProjectByIdWithSharedComponentsAndVersions(anyLong());
        
        verify(projectDao).findById(anyLong());
        verify(mockProject).getChildren();
        verify(mockProject).getComponents();
        verify(mockProject).getVersions();
    }
    
    @Test
    public void testGetAllProjects () {
    
        projectService.getAllProjects();
        
        verify(projectDao).findAll();
    }
    
    @Test
    public void testGetProjectByName () {
    
        projectService.getProjectByName("name");
        
        verify(projectDao).findByName("name");
    }
    
    @Test
    public void testGetProjectsBySection () {
    
        Section mockSection = mock(Section.class);
        
        projectService.getProjectsBySection(mockSection);
        
        verify(mockSection).getId();
        verify(projectDao).findBySection(anyLong());
    }
    
    @Test
    public void testGetProjectsByDevLeader () {
    
        User mockUser = mock(User.class);
        
        projectService.getProjectsByDevLeader(mockUser);
        
        verify(mockUser).getId();
        verify(projectDao).findByDevLeader(anyInt());
    }
    
    @Test
    public void testGetProjectsByQALeader () {
    
        User mockUser = mock(User.class);
        
        projectService.getProjectsByQALeader(mockUser);
        
        verify(mockUser).getId();
        verify(projectDao).findByQALeader(anyInt());
    }
    
    @Test
    public void testSaveProject () {
    
        projectService.saveProject(any(Project.class));
        
        verify(projectDao).saveOrUpdate(any(Project.class));
    }
    
    @Test
    public void testDeleteProject () {
    
        projectService.deleteProject(any(Project.class));
        
        verify(projectDao).delete(any(Project.class));
    }
    
    @Test
    public void testGetProjectVersionById () {
    
        projectService.getProjectVersionById(anyLong());
        
        verify(projectVersionDao).findById(anyLong());
    }
    
    @Test
    public void testGetProjectVersionByProject () {
    
        Project mockProject = mock(Project.class);
        
        projectService.getProjectVersionsByProject(mockProject);
        
        verify(mockProject).getId();
        verify(projectVersionDao).findByProject(anyLong());
    }
    
    @Test
    public void testSetHigherOrderToProjectVersion () {
    
        ProjectVersion mockProjectVersion = mock(ProjectVersion.class);
        
        projectService.setHigherOrderToProjectVersion(mockProjectVersion);
        
        verify(mockProjectVersion).getId();
        verify(projectVersionDao).decreaseSortValue(anyLong());
    }
    
    @Test
    public void testSetLowerOrderToProjectVersion () {
    
        ProjectVersion mockProjectVersion = mock(ProjectVersion.class);
        
        projectService.setLowerOrderToProjectVersion(mockProjectVersion);
        
        verify(mockProjectVersion).getId();
        verify(projectVersionDao).increaseSortValue(anyLong());
    }
    
    @Test
    public void testSaveProjectVersion () {
    
        projectService.saveProjectVersion(any(ProjectVersion.class));
        
        verify(projectVersionDao).saveOrUpdate(any(ProjectVersion.class));
    }
    
    @Test
    public void testDeleteProjectVersion () {
    
        projectService.deleteProjectVersion(any(ProjectVersion.class));
        
        verify(projectVersionDao).delete(any(ProjectVersion.class));
    }
    
    @Test
    public void testGetComponentById () {
    
        projectService.getComponentById(anyLong());
        
        verify(componentDao).findById(anyLong());
    }
    
    @Test
    public void testGetComponentByName () {
    
        projectService.getComponentByName("name");
        
        verify(componentDao).findByName("name");
    }
    
    @Test
    public void testGetComponentsByProject () {
    
        Project mockProject = mock(Project.class);
        
        projectService.getComponentsByProject(mockProject);
        
        verify(mockProject).getId();
        verify(componentDao).findByProject(anyLong());
    }
    
    @Test
    public void testSaveComponent () {
    
        projectService.saveComponent(any(Component.class));
        
        verify(componentDao).saveOrUpdate(any(Component.class));
    }
    
    @Test
    public void testDeleteComponent () {
    
        projectService.deleteComponent(any(Component.class));
        
        verify(componentDao).delete(any(Component.class));
    }
    
    @Test
    public void testGetComponentByIdWithSharedProjects_allOK () {
    
        Project project = ProjectBuilder.project().withId(1L).withName("project_name").build();
        Set<Project> projects = new HashSet<Project>(1);
        projects.add(project);
        
        Set<Project> spyProjects = spy(projects);
        
        Component component = ComponentBuilder.component()
                                              .withId(1L)
                                              .withName("component_name")
                                              .withProjects(spyProjects)
                                              .build();
        
        when(componentDao.findById(anyLong())).thenReturn(component);
        
        projectService.getComponentByIdWithSharedProjects(anyLong());
        
        verify(componentDao).findById(anyLong());
        verify(spyProjects).size();
    }
    
    @Test
    public void testGetComponentByIdWithSharedProjects_componentNotFound () {
    
        Set<Project> spyProjects = spy(new HashSet<Project>());
        
        when(componentDao.findById(anyLong())).thenReturn(null);
        
        projectService.getComponentByIdWithSharedProjects(anyLong());
        
        verify(componentDao).findById(anyLong());
        verify(spyProjects, never()).size();
    }
    
    @Test
    public void testGetShareableProjectsForComponent () {
    
        Component component = ComponentBuilder.component().withId(1L).withName("component_name").build();
        
        projectService.getShareableProjectsForComponent(component);
        
        verify(projectDao).findShareableProjectsForComponent(component.getId());
    }
    
    @Test
    public void testShareComponentWithProject () {
    
        Component component = ComponentBuilder.component().withId(1L).withName("component_name").build();
        Set<Component> spyComponents = spy(new HashSet<Component>());
        
        Project project = ProjectBuilder.project()
                                        .withId(1L)
                                        .withName("project_name")
                                        .withComponents(spyComponents)
                                        .build();
        
        projectService.shareComponentWithProject(component, project);
        
        verify(spyComponents).add(component);
        verify(projectDao).saveOrUpdate(project);
    }

    @Test
    public void testUnshareComponentWithProject () {
    
        Component component = ComponentBuilder.component().withId(1L).withName("component_name").build();
        Set<Component> spyComponents = spy(new HashSet<Component>());
        
        Project project = ProjectBuilder.project()
                                        .withId(1L)
                                        .withName("project_name")
                                        .withComponents(spyComponents)
                                        .build();
        
        projectService.unshareCompronentFromProject(component, project);
        
        verify(spyComponents).remove(component);
        verify(projectDao).saveOrUpdate(project);
    }
}
