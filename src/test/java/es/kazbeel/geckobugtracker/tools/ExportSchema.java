package es.kazbeel.geckobugtracker.tools;


import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;


public class ExportSchema {
    
    private static final String HIBERNATE_CFG_FILENAME = "hibernate.cfg.xml";
    private static final String SCHEMA_DB_OUTPUT       = "src/main/resources/gbt_schema.sql";
    
    
    public static void main (String[] args) {
        Configuration cfg = new Configuration();
        cfg.configure(HIBERNATE_CFG_FILENAME);
        cfg.setProperty("hibernate.dialect", Dialect.MYSQL.getDialectClass());
        
        SchemaExport se = new SchemaExport(cfg);
        se.setOutputFile(SCHEMA_DB_OUTPUT);
        se.create(true, false);
    }
    
    
    private static enum Dialect {
        
        MYSQL       ("org.hibernate.dialect.MySQLDialect"),
        HSQL        ("org.hibernate.dialect.HSQLDialect"),
        POSTGRESQL  ("org.hibernate.dialect.ProgressDialect");
        
        private String dialectClass;
        
        
        private Dialect (String dialectClass) {
            this.dialectClass = dialectClass;
        }
        
        public String getDialectClass () {
            return dialectClass;
        }
        
    }
    
}
