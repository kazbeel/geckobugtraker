package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.EnvironmentBuilder;
import es.kazbeel.geckobugtracker.model.Environment;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class EnvironmentDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private EnvironmentDao environmentDao;
    
    
    @Test
    @DatabaseSetup("EnvironmentDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<Environment> environments = environmentDao.findAll();
        
        assertThat(environments, is(not(nullValue())));
        assertThat(environments, hasSize(3));
        assertThat(environments, contains(EnvironmentBuilder.environment().withId(1).withName("environment_1").build(),
                                          EnvironmentBuilder.environment().withId(3).withName("environment_2").build(),
                                          EnvironmentBuilder.environment().withId(2).withName("environment_3").build()));
    }
    
    @Test
    @DatabaseSetup("EnvironmentDaoTest.xml")
    @Transactional
    public void testFindAllEnabled () {
    
        List<Environment> environments = environmentDao.findAllEnabled();
        
        assertThat(environments, is(not(nullValue())));
        assertThat(environments, hasSize(2));
        assertThat(environments, contains(EnvironmentBuilder.environment().withId(1).withName("environment_1").build(),
                                          EnvironmentBuilder.environment().withId(3).withName("environment_3").build()));
    }
    
    @Test
    @DatabaseSetup("EnvironmentDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        Environment environment = environmentDao.findById(1);
        
        assertThat(environment, is(not(nullValue())));
        assertThat(environment, is(EnvironmentBuilder.environment().withId(1).withName("environment_1").build()));
    }
}
