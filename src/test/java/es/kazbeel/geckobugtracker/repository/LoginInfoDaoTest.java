package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.LoginInfoBuilder;
import es.kazbeel.geckobugtracker.model.LoginInfo;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class LoginInfoDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private LoginInfoDao loginInfoDao;
    
    
    @Test
    @DatabaseSetup("LoginInfoDaoTest.xml")
    @Transactional
    public void testFindByUsernameFound () {
    
        LoginInfo loginInfo = loginInfoDao.findByUsername("login_name_1");
        
        assertThat(loginInfo, is(not(nullValue())));
        assertThat(loginInfo, is(LoginInfoBuilder.loginInfo().withId(1).withUsername("login_name_1").build()));
    }
    
    @Test
    @DatabaseSetup("LoginInfoDaoTest.xml")
    @Transactional
    public void testFindByUsernameNotFound () {
    
        LoginInfo loginInfo = loginInfoDao.findByUsername("unknown");
        
        assertThat(loginInfo, is(nullValue()));
    }
    
    @Test
    @DatabaseSetup("LoginInfoDaoTest.xml")
    @Transactional
    public void testResetFailAttempts () {
    
        loginInfoDao.resetFailAttempts("login_name_2");
        LoginInfo loginInfo = loginInfoDao.findByUsername("login_name_2");
        
        assertThat(loginInfo, is(not(nullValue())));
        assertThat(loginInfo, is(LoginInfoBuilder.loginInfo().withId(2).withUsername("login_name_2").build()));
    }
}
