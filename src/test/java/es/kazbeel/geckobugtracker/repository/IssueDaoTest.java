package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.IssueBuilder;
import es.kazbeel.geckobugtracker.model.Issue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class IssueDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private IssueDao issueDao;
    
    
    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        Issue issue = issueDao.findById(2);
        
        assertThat(issue, is(not(nullValue())));
        assertThat(issue, is(IssueBuilder.issue().withId(2).build()));
    }
    
    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testFindByAssigneeAndResolutionAllOK () {
        
        List<Issue> issues = issueDao.findByAssigneeAndResolution(2, 1, 0, 100);
        
        assertThat(issues, is(not(nullValue())));
        assertThat(issues, hasSize(2));
        assertThat(issues, contains(IssueBuilder.issue().withId(1).build(),
                                    IssueBuilder.issue().withId(3).build()));
    }

    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testFindByAssigneeAndResolutionAssigneeNotExist () {
        
        List<Issue> issues = issueDao.findByAssigneeAndResolution(0, 1, 0, 100);
        
        assertThat(issues, is(not(nullValue())));
        assertThat(issues, is(empty()));
    }

    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testFindByAssigneeAndResolutionResolutionNotExist () {
        
        List<Issue> issues = issueDao.findByAssigneeAndResolution(1, 0, 0, 100);
        
        assertThat(issues, is(not(nullValue())));
        assertThat(issues, is(empty()));
    }

    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testFindAll () {
        
        List<Issue> issues = issueDao.findAll(0, 100);
        
        assertThat(issues, is(not(nullValue())));
        assertThat(issues, hasSize(3));
    }

    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testFindByReporter () {
        
        List<Issue> issues = issueDao.findByReporter(1, 0, 100);
        
        assertThat(issues, is(not(nullValue())));
        assertThat(issues, hasSize(2));
        assertThat(issues, contains(IssueBuilder.issue().withId(1).build(),
                                    IssueBuilder.issue().withId(3).build()));
    }

    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testFindByReporterNotExist () {
        
        List<Issue> issues = issueDao.findByReporter(0, 0, 100);
        
        assertThat(issues, is(not(nullValue())));
        assertThat(issues, is(empty()));
    }

    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testCountByReporter () {
        
        Long count = issueDao.countByReporter(1);
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(2)));
    }

    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testCountByReporterNotExist () {
        
        Long count = issueDao.countByReporter(0);
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(0)));
    }

    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testCountByAssigneeAndResolution () {
        
        Long count = issueDao.countByAssigneeAndResolution(1, 1);
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(1)));
    }

    @Test
    @DatabaseSetup("IssueDaoTest.xml")
    @Transactional
    public void testCountAll () {
        
        Long count = issueDao.countAll();
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(3)));
    }
}
