package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.StatusBuilder;
import es.kazbeel.geckobugtracker.model.Status;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class StatusDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private StatusDao statusDao;
    
    
    @Test
    @DatabaseSetup("StatusDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<Status> statuses = statusDao.findAll();
        
        assertThat(statuses, is(not(nullValue())));
        assertThat(statuses, hasSize(3));
        assertThat(statuses, contains(StatusBuilder.status().withId(1).withName("status_1").build(),
                                      StatusBuilder.status().withId(3).withName("status_3").build(),
                                      StatusBuilder.status().withId(2).withName("status_2").build()));
    }
    
    @Test
    @DatabaseSetup("StatusDaoTest.xml")
    @Transactional
    public void testFindAllEnabled () {
    
        List<Status> statuses = statusDao.findAllEnabled();
        
        assertThat(statuses, is(not(nullValue())));
        assertThat(statuses, hasSize(2));
        assertThat(statuses, contains(StatusBuilder.status().withId(1).withName("status_1").build(),
                                      StatusBuilder.status().withId(2).withName("status_2").build()));
    }
    
    @Test
    @DatabaseSetup("StatusDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        Status status = statusDao.findById(1);
        
        assertThat(status, is(not(nullValue())));
        assertThat(status, is(StatusBuilder.status().withId(1).withName("status_1").build()));
    }
}
