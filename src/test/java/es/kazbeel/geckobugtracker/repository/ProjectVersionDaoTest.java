package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.ProjectVersionBuilder;
import es.kazbeel.geckobugtracker.model.ProjectVersion;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class ProjectVersionDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private ProjectVersionDao projectVersionDao;
    
    
    @Test
    @DatabaseSetup("ProjectVersionDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        ProjectVersion projectVersion = projectVersionDao.findById(1L);
        
        assertThat(projectVersion, is(not(nullValue())));
        assertThat(projectVersion, is(ProjectVersionBuilder.projectVersion()
                                                           .withId(1L)
                                                           .withName("version_name_1")
                                                           .build()));
    }
    
    @Test
    @DatabaseSetup("ProjectVersionDaoTest.xml")
    @Transactional
    public void testFindByProject () {
    
        List<ProjectVersion> projectVersions = projectVersionDao.findByProject(1L);
        
        assertThat(projectVersions, is(not(nullValue())));
        assertThat(projectVersions, hasSize(2));
        assertThat(projectVersions,
                   contains(ProjectVersionBuilder.projectVersion().withId(1L).withName("version_name_1").build(),
                            ProjectVersionBuilder.projectVersion().withId(2L).withName("version_name_2").build()));
    }
    
    @Test
    @DatabaseSetup("ProjectVersionDaoTest.xml")
    @Transactional
    public void testIncreaseSortValue () {
    
        ProjectVersion projectVersion = (ProjectVersion) sessionFactory.getCurrentSession().get(ProjectVersion.class,
                                                                                                1L);
        
        assertThat(projectVersion, is(not(nullValue())));
        assertThat(projectVersion.getSortValue(), is(0));
        
        sessionFactory.getCurrentSession().clear();
        
        projectVersionDao.increaseSortValue(1L);
        
        projectVersion = (ProjectVersion) sessionFactory.getCurrentSession().get(ProjectVersion.class, 1L);
        
        assertThat(projectVersion, is(not(nullValue())));
        assertThat(projectVersion.getSortValue(), is(1));
    }
    
    @Test
    @DatabaseSetup("ProjectVersionDaoTest.xml")
    @Transactional
    public void testDecreaseSortValue () {
    
        ProjectVersion projectVersion = (ProjectVersion) sessionFactory.getCurrentSession().get(ProjectVersion.class,
                                                                                                2L);
        
        assertThat(projectVersion, is(not(nullValue())));
        assertThat(projectVersion.getSortValue(), is(1));
        
        sessionFactory.getCurrentSession().clear();
        
        projectVersionDao.decreaseSortValue(2L);
        
        projectVersion = (ProjectVersion) sessionFactory.getCurrentSession().get(ProjectVersion.class, 2L);
        
        assertThat(projectVersion, is(not(nullValue())));
        assertThat(projectVersion.getSortValue(), is(0));
    }
    
    @Test
    @DatabaseSetup("ProjectVersionDaoTest.xml")
    @Transactional
    public void testDecreaseSortValue_valueIsAlwaysGreaterThanZero () {
    
        ProjectVersion projectVersion = (ProjectVersion) sessionFactory.getCurrentSession().get(ProjectVersion.class,
                                                                                                1L);
        
        assertThat(projectVersion, is(not(nullValue())));
        assertThat(projectVersion.getSortValue(), is(0));
        
        sessionFactory.getCurrentSession().clear();
        
        projectVersionDao.decreaseSortValue(1L);
        
        projectVersion = (ProjectVersion) sessionFactory.getCurrentSession().get(ProjectVersion.class, 1L);
        
        assertThat(projectVersion, is(not(nullValue())));
        assertThat(projectVersion.getSortValue(), is(0));
    }
}
