package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.UserBuilder;
import es.kazbeel.geckobugtracker.model.User;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class UserDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private UserDao userDao;
    
    
    @Test
    @DatabaseSetup("UserDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<User> users = userDao.findAll(0, 2);
        
        assertThat(users, is(not(nullValue())));
        assertThat(users, hasSize(2));
        assertThat(users,
                   contains(UserBuilder.user().withId(1).withLoginName("login_name_1").build(),
                            UserBuilder.user().withId(2).withLoginName("login_name_2").build()));
        
        users = userDao.findAll(2, 2);
        
        assertThat(users, is(not(nullValue())));
        assertThat(users, hasSize(1));
        assertThat(users, contains(UserBuilder.user().withId(3).withLoginName("login_name_3").build()));
    }
    
    @Test
    @DatabaseSetup("UserDaoTest.xml")
    @Transactional
    public void testFindByIdFound () {
    
        User user = userDao.findById(2);
        
        assertThat(user, is(not(nullValue())));
        assertThat(user, is(UserBuilder.user().withId(2).withLoginName("login_name_2").build()));
    }
    
    @Test
    @DatabaseSetup("UserDaoTest.xml")
    @Transactional
    public void testFindByIdNotFound () {
    
        User user = userDao.findById(0);
        
        assertThat(user, is(nullValue()));
    }
    
    @Test
    @DatabaseSetup("UserDaoTest.xml")
    @Transactional
    public void testFindByLoginName () {
    
        User user = userDao.findByLoginName("login_name_1");
        
        assertThat(user, is(not(nullValue())));
        assertThat(user, is(UserBuilder.user().withId(1).withLoginName("login_name_1").build()));
    }
    
    @Test
    @SuppressWarnings("unchecked")
    @DatabaseSetup("UserDaoTest.xml")
    @Transactional
    public void testFindAllEnabled () {
    
        List<User> users = userDao.findAllEnabled();
        
        assertThat(users, is(not(nullValue())));
        assertThat(users, hasSize(2));
        assertThat(users, contains(hasProperty("enabled", is(true)), hasProperty("enabled", is(true))));
    }
    
    @Test
    @DatabaseSetup("UserDaoTest.xml")
    @Transactional
    public void testCountAll () {
    
        Long count = userDao.countAll();
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(3)));
        
    }
}
