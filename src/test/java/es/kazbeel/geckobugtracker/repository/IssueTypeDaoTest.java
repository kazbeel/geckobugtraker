package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.IssueTypeBuilder;
import es.kazbeel.geckobugtracker.model.IssueType;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class IssueTypeDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private IssueTypeDao issueTypeDao;
    
    
    @Test
    @DatabaseSetup("IssueTypeDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<IssueType> issueTypes = issueTypeDao.findAll();
        
        assertThat(issueTypes, is(not(nullValue())));
        assertThat(issueTypes, hasSize(3));
        assertThat(issueTypes, contains(IssueTypeBuilder.issueType().withId(1).withName("issue_type_1").build(),
                                        IssueTypeBuilder.issueType().withId(2).withName("issue_type_2").build(),
                                        IssueTypeBuilder.issueType().withId(3).withName("issue_type_3").build()));
    }
    
    @Test
    @DatabaseSetup("IssueTypeDaoTest.xml")
    @Transactional
    public void testFindAllEnabled () {
    
        List<IssueType> issueTypes = issueTypeDao.findAllEnabled();
        
        assertThat(issueTypes, is(not(nullValue())));
        assertThat(issueTypes, hasSize(2));
        assertThat(issueTypes, contains(IssueTypeBuilder.issueType().withId(1).withName("issue_type_1").build(),
                                        IssueTypeBuilder.issueType().withId(3).withName("issue_type_3").build()));
    }
    
    @Test
    @DatabaseSetup("IssueTypeDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        IssueType issueType = issueTypeDao.findById(1);
        
        assertThat(issueType, is(not(nullValue())));
        assertThat(issueType, is(IssueTypeBuilder.issueType().withId(1).withName("issue_type_1").build()));
    }
}
