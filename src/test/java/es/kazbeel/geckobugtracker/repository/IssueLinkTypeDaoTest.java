package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.IssueLinkTypeBuilder;
import es.kazbeel.geckobugtracker.model.IssueLinkType;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class IssueLinkTypeDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private IssueLinkTypeDao issueLinkTypeDao;
    
    
    @Test
    @DatabaseSetup("IssueLinkTypeDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<IssueLinkType> issueLinkTypes = issueLinkTypeDao.findAll();
        
        assertThat(issueLinkTypes, is(not(nullValue())));
        assertThat(issueLinkTypes, hasSize(3));
        assertThat(issueLinkTypes, contains(IssueLinkTypeBuilder.issueLinkType().withId(1).withName("name_1").build(),
                                            IssueLinkTypeBuilder.issueLinkType().withId(2).withName("name_2").build(),
                                            IssueLinkTypeBuilder.issueLinkType().withId(3).withName("name_3").build()));
    }

    @Test
    @DatabaseSetup("IssueLinkTypeDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        IssueLinkType issueLinkType = issueLinkTypeDao.findById(1);
        
        assertThat(issueLinkType, is(not(nullValue())));
        assertThat(issueLinkType, is(IssueLinkTypeBuilder.issueLinkType().withId(1).withName("name_1").build()));
    }
}
