package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.ComponentBuilder;
import es.kazbeel.geckobugtracker.model.Component;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class ComponentDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private ComponentDao componentDao;
    
    
    @Test
    @DatabaseSetup("ComponentDaoTest.xml")
    @Transactional
    public void testFindById () {
        
        Component component = componentDao.findById(3L);
        
        assertThat(component, is(not(nullValue())));
        assertThat(component, is(ComponentBuilder.component().withId(3L).withName("component_name_3").build()));
    }
    
    @Test
    @DatabaseSetup("ComponentDaoTest.xml")
    @Transactional
    public void testFindByName () {
    
        Component component = componentDao.findByName("component_name_3");
        
        assertThat(component, is(not(nullValue())));
        assertThat(component, is(ComponentBuilder.component().withId(3L).withName("component_name_3").build()));
    }

    @Test
    @DatabaseSetup("ComponentDaoTest.xml")
    @Transactional
    public void testFindByProject () {
    
        List<Component> components = componentDao.findByProject(2L);
        
        assertThat(components, is(not(nullValue())));
        assertThat(components, hasSize(3));
        assertThat(components, contains(ComponentBuilder.component().withId(2L).withName("component_name_2").build(),
                                        ComponentBuilder.component().withId(3L).withName("component_name_3").build(),
                                        ComponentBuilder.component().withId(4L).withName("component_name_4").build()));
    }
}
