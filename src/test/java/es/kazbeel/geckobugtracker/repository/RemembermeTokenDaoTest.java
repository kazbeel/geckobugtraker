package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.RemembermeTokenBuilder;
import es.kazbeel.geckobugtracker.model.RemembermeToken;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class RemembermeTokenDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private RemembermeTokenDao remembermeTokenDao;
    
    
    @Test
    @DatabaseSetup("RemembermeTokenDaoTest.xml")
    @Transactional
    public void testFindByUsername () {
    
        List<RemembermeToken> rmt = remembermeTokenDao.findByUsername("username_1");
        
        assertThat(rmt, is(not(nullValue())));
        assertThat(rmt,
                   contains(RemembermeTokenBuilder.remembermeToken()
                                                  .withId(1)
                                                  .withUsername("username_1")
                                                  .withSeries("series_1")
                                                  .build(),
                            RemembermeTokenBuilder.remembermeToken()
                                                  .withId(2)
                                                  .withUsername("username_1")
                                                  .withSeries("series_2")
                                                  .build()));
    }
    
    @Test
    @DatabaseSetup("RemembermeTokenDaoTest.xml")
    @Transactional
    public void testFindBySeries () {
    
        RemembermeToken rmt = remembermeTokenDao.findBySeries("series_2");
        
        assertThat(rmt, is(not(nullValue())));
        assertThat(rmt,
                   is(RemembermeTokenBuilder.remembermeToken()
                                            .withId(2)
                                            .withUsername("username_1")
                                            .withSeries("series_2")
                                            .build()));
    }
    
    @Test(expected = DataIntegrityViolationException.class)
    @DatabaseSetup("RemembermeTokenDaoTest.xml")
    @Transactional
    public void testUsernameSeriesUniqueConstraint () {
    
        remembermeTokenDao.save(RemembermeTokenBuilder.remembermeToken()
                                                      .withUsername("username_a")
                                                      .withSeries("series_string")
                                                      .withToken("token_string_a")
                                                      .build());
        remembermeTokenDao.save(RemembermeTokenBuilder.remembermeToken()
                                                      .withUsername("username_a")
                                                      .withSeries("series_string")
                                                      .withToken("token_string_b")
                                                      .build());
    }
}
