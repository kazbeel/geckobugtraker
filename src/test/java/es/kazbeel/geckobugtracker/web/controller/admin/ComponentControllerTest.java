package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.builders.ComponentBuilder;
import es.kazbeel.geckobugtracker.builders.ProjectBuilder;
import es.kazbeel.geckobugtracker.model.Component;
import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.service.ProjectService;
import es.kazbeel.geckobugtracker.service.UserGroupService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class ComponentControllerTest {
    
    @Mock
    private ProjectService projectService;
    
    @Mock
    private UserGroupService userGroupService;
    
    @InjectMocks
    private ComponentController componentController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(componentController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testCreateComponentForm () throws Exception {
    
        when(projectService.getProjectById(anyLong())).thenReturn(new Project());
        
        mockMvc.perform(post("/admin/components/CreateComponentForm").param("projectId", "1"))
               .andExpect(view().name("/admin/components/CreateComponent"))
               .andExpect(model().size(4))
               .andExpect(model().attributeExists("projectId"))
               .andExpect(model().attributeExists("users"))
               .andExpect(model().attributeExists("project"))
               .andExpect(model().attributeExists("component"));
        
        verify(userGroupService).getAllEnabledUsers();
        verify(projectService).getProjectById(anyLong());
    }
    
    @Test
    public void testCreateComponentPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/components/CreateComponent").param("id", "1")
                                                                 .param("name", "component_name")
                                                                 .param("devLeader.id", "2")
                                                                 .param("qaLeader.id", "3")
                                                                 .param("rootProject.id", "4"))
               .andExpect(redirectedUrl("/admin/projects/DetailsProject?id=4"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attribute("id", "4"));
        
        verify(projectService).saveComponent(any(Component.class));
    }
    
    @Test
    public void testCreateComponentPost_withBindingResultError () throws Exception {
    
        mockMvc.perform(post("/admin/components/CreateComponent"))
               .andExpect(view().name("/admin/components/CreateComponent"))
               .andExpect(model().hasErrors());
        
        verify(projectService, never()).saveComponent(any(Component.class));
    }
    
    @Test
    public void testCreateComponentPost_throwsDataIntegrityViolationException () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService).saveComponent(any(Component.class));
        
        mockMvc.perform(post("/admin/components/CreateComponent").param("id", "1")
                                                                 .param("name", "component_name")
                                                                 .param("devLeader.id", "2")
                                                                 .param("qaLeader.id", "3")
                                                                 .param("rootProject.id", "4"))
               .andExpect(view().name("/admin/components/CreateComponent"))
               .andExpect(model().hasNoErrors());
        
        verify(projectService).saveComponent(any(Component.class));
    }
    
    @Test
    public void testEditComponentGet () throws Exception {
    
        when(projectService.getComponentById(anyLong())).thenReturn(new Component());
        
        mockMvc.perform(get("/admin/components/EditComponent").param("id", "1"))
               .andExpect(view().name("/admin/components/EditComponent"))
               .andExpect(model().size(3))
               .andExpect(model().attributeExists("users"))
               .andExpect(model().attributeExists("projects"))
               .andExpect(model().attributeExists("component"));
        
        verify(userGroupService).getAllEnabledUsers();
        verify(projectService).getAllProjects();
        verify(projectService).getComponentById(anyLong());
    }
    
    @Test
    public void testEditComponentPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/components/EditComponent").param("id", "1")
                                                               .param("name", "component_name")
                                                               .param("devLeader.id", "2")
                                                               .param("qaLeader.id", "3")
                                                               .param("rootProject.id", "4"))
               .andExpect(redirectedUrl("/admin/components/DetailsComponent?id=1"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attribute("id", "1"));
        
        verify(projectService).saveComponent(any(Component.class));
    }
    
    @Test
    public void testEditComponentPost_withBindingResultError () throws Exception {
    
        mockMvc.perform(post("/admin/components/EditComponent").param("id", "1"))
               .andExpect(view().name("/admin/components/EditComponent"))
               .andExpect(model().hasErrors());
        
        verify(projectService, never()).saveComponent(any(Component.class));
    }
    
    @Test
    public void testEditComponentPost_throwsDataIntegrityViolationException () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService).saveComponent(any(Component.class));
        
        mockMvc.perform(post("/admin/components/EditComponent").param("id", "1")
                                                               .param("name", "component_name")
                                                               .param("devLeader.id", "2")
                                                               .param("qaLeader.id", "3")
                                                               .param("rootProject.id", "4"))
               .andExpect(view().name("/admin/components/EditComponent"))
               .andExpect(model().hasNoErrors());
        
        verify(projectService).saveComponent(any(Component.class));
    }
    
    @Test
    public void testDeleteComponent_allOK () throws Exception {
    
        Project dummyProject = ProjectBuilder.project().withId(2L).withName("project_name").build();
        Component dummyComponent = ComponentBuilder.component()
                                                   .withId(1L)
                                                   .withName("component_name")
                                                   .withRootProject(dummyProject)
                                                   .build();
        
        when(projectService.getComponentById(anyLong())).thenReturn(dummyComponent);
        
        mockMvc.perform(get("/admin/components/DeleteComponent").param("id", "1"))
               .andExpect(redirectedUrl("/admin/projects/DetailsProject?id=2"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("id"))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(projectService).getComponentById(anyLong());
        verify(projectService).deleteComponent(any(Component.class));
    }
    
    @Test
    public void testDeleteComponent_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(projectService).deleteComponent(any(Component.class));
        
        mockMvc.perform(get("/admin/components/DeleteComponent").param("id", "1"))
               .andExpect(redirectedUrl("/admin/projects/ViewProjects"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(projectService).getComponentById(anyLong());
        verify(projectService).deleteComponent(any(Component.class));
    }
    
    @Test
    public void testDeleteComponent_throwsDataIntegrityViolationException () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService).deleteComponent(any(Component.class));
        
        mockMvc.perform(get("/admin/components/DeleteComponent").param("id", "1"))
               .andExpect(redirectedUrl("/admin/projects/ViewProjects"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(projectService).getComponentById(anyLong());
        verify(projectService).deleteComponent(any(Component.class));
    }
    
    @Test
    public void testDetailsComponent_allOK () throws Exception {
    
        when(projectService.getComponentByIdWithSharedProjects(anyLong())).thenReturn(new Component());
        
        mockMvc.perform(get("/admin/components/DetailsComponent").param("id", "1"))
               .andExpect(view().name("/admin/components/DetailsComponent"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("component"));
        
        verify(projectService).getComponentByIdWithSharedProjects(anyLong());
    }
    
    @Test
    public void testDetailsComponent_componentDoesNotExist () throws Exception {
    
        when(projectService.getComponentByIdWithSharedProjects(anyLong())).thenReturn(null);
        
        mockMvc.perform(get("/admin/components/DetailsComponent").param("id", "1"))
               .andExpect(status().isNotFound());
        
        verify(projectService).getComponentByIdWithSharedProjects(anyLong());
    }
}
