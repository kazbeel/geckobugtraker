package es.kazbeel.geckobugtracker.web.controller;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.support.StaticApplicationContext;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.Attachment;
import es.kazbeel.geckobugtracker.model.Comment;
import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.IssueLink;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.controller.exception.GlobalExceptionController;
import es.kazbeel.geckobugtracker.web.secure.CurrentUserDetails;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class IssueDetailsControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @Mock
    private UserGroupService userGroupService;
    
    @InjectMocks
    private IssueDetailsController issueDetailsController;
    
    private MockMvc mockMvc;
    
    private User admin = new User();
    
    
    @Before
    public void setup () {
    
        CurrentUserDetails loggedUser = new CurrentUserDetails("admin",
                                                               "admin",
                                                               AuthorityUtils.createAuthorityList("ROLE_ADMIN"),
                                                               admin);
        Authentication auth = new UsernamePasswordAuthenticationToken(loggedUser, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        final ExceptionHandlerExceptionResolver exceptionHandlerExceptionResolver = new ExceptionHandlerExceptionResolver();
        
        // here we need to setup a dummy application context that only registers the GlobalControllerExceptionHandler
        final StaticApplicationContext applicationContext = new StaticApplicationContext();
        applicationContext.registerBeanDefinition("advice", new RootBeanDefinition(GlobalExceptionController.class,
                                                                                   null,
                                                                                   null));
        
        // set the application context of the resolver to the dummy application context we just created
        exceptionHandlerExceptionResolver.setApplicationContext(applicationContext);
        
        // needed in order to force the exception resolver to update it's internal caches
        exceptionHandlerExceptionResolver.afterPropertiesSet();
        
        mockMvc = MockMvcBuilders.standaloneSetup(issueDetailsController)
                                 .setViewResolvers(viewResolver)
                                 .setHandlerExceptionResolvers(exceptionHandlerExceptionResolver)
                                 .build();
    }
    
    @Test
    public void testIssueDetailsGet_AllOK () throws Exception {
    
        when(issueService.getIssueById(anyInt())).thenReturn(new Issue());
        
        mockMvc.perform(get("/issues/0").param("issueId", "0"))
               .andExpect(view().name("issues/Details"))
               .andExpect(model().size(13))
               .andExpect(model().attributeExists("issuePackage"))
               .andExpect(model().attributeExists("loggedUser"))
               .andExpect(model().attributeExists("issue"))
               .andExpect(model().attributeExists("numVotes"))
               .andExpect(model().attributeExists("isVoter"))
               .andExpect(model().attributeExists("numWatchers"))
               .andExpect(model().attributeExists("isWatcher"))
               .andExpect(model().attributeExists("comment"))
               .andExpect(model().attributeExists("outIssueLinks"))
               .andExpect(model().attributeExists("inIssueLinks"))
               .andExpect(model().attributeExists("allIssueLinkTypes"))
               .andExpect(model().attributeExists("newIssueLink"))
               .andExpect(model().attributeExists("attachments"));
    }
    
    @Test
    public void testIssueDetailsGet_IssueIdNOK () throws Exception {
    
        when(issueService.getIssueById(anyInt())).thenReturn(null);
        
        mockMvc.perform(get("/issues/0").param("issueId", "0")).andExpect(redirectedUrl("/Dashboard"));
    }
    
    @Test
    public void testAddComment_AllOK () throws Exception {
    
        when(issueService.getIssueById(anyInt())).thenReturn(new Issue());
        
        mockMvc.perform(post("/issues/0").param("body", "...")
                                         .param("author.loginName", "login_name")
                                         .param("author.password", "password")
                                         .param("author.firstName", "first_name")
                                         .param("author.lastName", "last_name")
                                         .param("author.email", "email@email.com")
                                         .param("reporter.loginName", "login_name")
                                         .param("reporter.password", "password")
                                         .param("reporter.firstName", "first_name")
                                         .param("reporter.lastName", "last_name")
                                         .param("reporter.email", "email@email.com")
                                         .param("type.name", "type_name")
                                         .param("type.description", "type_description")
                                         .param("issue.summary", "summary"))
               .andExpect(redirectedUrl("/issues/0"))
               .andExpect(model().hasNoErrors());
        
        verify(issueService).addCommentToIssue(any(Issue.class), eq(admin), any(Comment.class));
    }
    
    @Test
    public void testAddComment_BodyNull () throws Exception {
    
        when(issueService.getIssueById(anyInt())).thenReturn(new Issue());
        
        mockMvc.perform(post("/issues/0")).andExpect(redirectedUrl("/issues/0")).andExpect(model().hasErrors());
        
        verify(issueService, never()).addCommentToIssue(any(Issue.class), eq(admin), any(Comment.class));
    }
    
    @Test
    public void testAddComment_BodyEmpty () throws Exception {
    
        when(issueService.getIssueById(anyInt())).thenReturn(new Issue());
        
        mockMvc.perform(post("/issues/0").param("body", ""))
               .andExpect(redirectedUrl("/issues/0"))
               .andExpect(model().hasErrors());
        
        verify(issueService, never()).addCommentToIssue(any(Issue.class), eq(admin), any(Comment.class));
    }
    
    @Test
    public void testVoteIssue_actionVoteOK () throws Exception {
    
        mockMvc.perform(get("/issues/0/vote").param("action", "vote")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).voteIssue(eq(admin), any(Issue.class), eq(true));
    }
    
    @Test
    public void testVoteIssue_actionUnvoteOK () throws Exception {
    
        mockMvc.perform(get("/issues/0/vote").param("action", "unvote")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).voteIssue(eq(admin), any(Issue.class), eq(false));
    }
    
    @Test
    public void testVoteIssue_actionNOK () throws Exception {
    
        mockMvc.perform(get("/issues/0/vote").param("action", "unknown")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService, never()).voteIssue(any(User.class), any(Issue.class), anyBoolean());
    }
    
    @Test
    public void testWatchIssue_actionWatchOK () throws Exception {
    
        mockMvc.perform(get("/issues/0/watch").param("action", "watch")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).watchIssue(eq(admin), any(Issue.class), eq(true));
    }
    
    @Test
    public void testWatchIssue_actionUnwatchOK () throws Exception {
    
        mockMvc.perform(get("/issues/0/watch").param("action", "unwatch")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).watchIssue(eq(admin), any(Issue.class), eq(false));
    }
    
    @Test
    public void testWatchIssue_actionNOK () throws Exception {
    
        mockMvc.perform(get("/issues/0/watch").param("action", "unknown")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService, never()).watchIssue(any(User.class), any(Issue.class), anyBoolean());
    }
    
    @Test
    public void testDeleteIssue () throws Exception {
    
        mockMvc.perform(get("/issues/0/delete")).andExpect(redirectedUrl("/"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).deleteIssue(any(Issue.class));
    }
    
    @Test
    public void testCreateIssueLink_allOK () throws Exception {
    
        mockMvc.perform(post("/issues/0/CreateIssueLink").param("linkType.id", "0")
                                                         .param("source.id", "0")
                                                         .param("destination.id", "1"))
               .andExpect(model().hasNoErrors())
               .andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).saveIssueLink(any(IssueLink.class));
    }
    
    @Test
    public void testCreateIssueLink_sourceAndDestinationMustBeDifferent () throws Exception {
    
        mockMvc.perform(post("/issues/0/CreateIssueLink").param("linkType.id", "0")
                                                         .param("source.id", "0")
                                                         .param("destination.id", "0"))
               .andExpect(model().hasErrors())
               .andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService, never()).saveIssueLink(any(IssueLink.class));
    }

    @Test
    public void testDeleteIssueLink_allOK () throws Exception {
        
        when(issueService.getIssueLinkById(anyInt())).thenReturn(new IssueLink());
        
        mockMvc.perform(post("/issues/0/DeleteIssueLink").param("issueLinkId", "1"))
        .andExpect(model().hasNoErrors())
        .andExpect(model().size(0))
        .andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueLinkById(anyInt());
        verify(issueService).deleteIssueLink(any(IssueLink.class));
    }
    
    @Test
    public void testDeleteIssueLink_issueLinkNotExist () throws Exception {
        
        when(issueService.getIssueLinkById(anyInt())).thenReturn(null);
        
        mockMvc.perform(post("/issues/0/DeleteIssueLink").param("issueLinkId", "1"))
        .andExpect(model().hasNoErrors())
        .andExpect(model().size(0))
        .andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueLinkById(anyInt());
        verify(issueService, never()).deleteIssueLink(any(IssueLink.class));
    }

    @Test
    public void testUploadAttachment_allOK () throws Exception {
    
        MockMultipartFile file = new MockMultipartFile("file", "original_filename.ext", null, "data".getBytes());
        
        mockMvc.perform(fileUpload("/issues/0/UploadAttachment").file(file))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("success"))
               .andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).saveAttachment(any(Attachment.class));
    }
    
    @Test
    public void testUploadAttachment_fileIsEmpty () throws Exception {
    
        MockMultipartFile file = new MockMultipartFile("file", null, null, "data".getBytes());
        
        mockMvc.perform(fileUpload("/issues/0/UploadAttachment").file(file))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("error"))
               .andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService, never()).saveAttachment(any(Attachment.class));
    }
    
    @Test
    public void testUploadAttachment_ioException () throws Exception {
    
        MockMultipartFile file = new MockMultipartFile("file", "original_filename.ext", null, "".getBytes());
        
        mockMvc.perform(fileUpload("/issues/0/UploadAttachment").file(file))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("error"))
               .andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService, never()).saveAttachment(any(Attachment.class));
    }
    
    @Test
    public void testDownloadAttachment_knownFileMimetype () throws Exception {
    
        Attachment attch = mock(Attachment.class);
        
        when(issueService.getAttachmentById(anyInt())).thenReturn(attch);
        when(attch.getFileMimeType()).thenReturn("multipart/form-data");
        when(attch.getFileName()).thenReturn("filename.ext");
        
        File dummyFile = new File("D:\\Programas\\Tomcat 7.0\\tmpFiles\\filename.ext");
        dummyFile.createNewFile();
        assertThat(dummyFile.exists(), is(true));
        
        mockMvc.perform(get("/issues/0/DownloadAttachment/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType("multipart/form-data"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).getAttachmentById(anyInt());
        
        dummyFile.delete();
    }
    
    @Test
    public void testDownloadAttachment_unknownFileMimetype () throws Exception {
    
        Attachment attch = mock(Attachment.class);
        
        when(issueService.getAttachmentById(anyInt())).thenReturn(attch);
        when(attch.getFileMimeType()).thenReturn(null);
        when(attch.getFileName()).thenReturn("filename.ext");
        
        File dummyFile = new File("D:\\Programas\\Tomcat 7.0\\tmpFiles\\filename.ext");
        dummyFile.createNewFile();
        assertThat(dummyFile.exists(), is(true));
        
        mockMvc.perform(get("/issues/0/DownloadAttachment/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType("application/octet-stream"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).getAttachmentById(anyInt());
        
        dummyFile.delete();
    }
    
    @Test
    public void testDownloadAttachment_attachmentNotFound () throws Exception {
    
        when(issueService.getAttachmentById(anyInt())).thenReturn(null);
        
        mockMvc.perform(get("/issues/0/DownloadAttachment/1")).andDo(print())
               .andExpect(status().isNotFound())
               .andExpect(view().name("errors/GenericError"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).getAttachmentById(anyInt());
    }
    
    @Test
    public void testDownloadAttachment_fileNotFound () throws Exception {
    
        when(issueService.getAttachmentById(anyInt())).thenReturn(new Attachment());
        
        mockMvc.perform(get("/issues/0/DownloadAttachment/1"))
               .andExpect(status().isNotFound())
               .andExpect(view().name("errors/GenericError"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).getAttachmentById(anyInt());
    }
}
