package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.IssueLinkType;
import es.kazbeel.geckobugtracker.service.IssueService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class IssueLinkTypeControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @InjectMocks
    private IssueLinkTypeController issueLinkTypeController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(issueLinkTypeController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewIssueLinkTypes () throws Exception {
    
        mockMvc.perform(get("/admin/issuelinktypes/ViewIssueLinkTypes"))
               .andExpect(view().name("/admin/issuelinktypes/ViewIssueLinkTypes"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueLinkTypes"));
        
        verify(issueService).getAllIssueLinkTypes();
    }
    
    @Test
    public void testCreateIssueLinkTypeGet () throws Exception {
    
        mockMvc.perform(get("/admin/issuelinktypes/CreateIssueLinkType"))
               .andExpect(view().name("/admin/issuelinktypes/CreateIssueLinkType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueLinkType"));
    }
    
    @Test
    public void testCreateIssueLinkTypePost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/issuelinktypes/CreateIssueLinkType").param("name", "not_empty")
                                                                         .param("inward", "inward")
                                                                         .param("outward", "outward"))
               .andExpect(redirectedUrl("/admin/issuelinktypes/ViewIssueLinkTypes"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueLinkType"));
        
        verify(issueService).saveIssueLinkType(any(IssueLinkType.class));
    }
    
    @Test
    public void testCreateIssueLinkTypePost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/issuelinktypes/CreateIssueLinkType").param("name", ""))
               .andExpect(view().name("/admin/issuelinktypes/CreateIssueLinkType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueLinkType"));
        
        verify(issueService, never()).saveIssueLinkType(any(IssueLinkType.class));
    }
    
    @Test
    public void testCreateIssueLinkTypePost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveIssueLinkType(any(IssueLinkType.class));
        
        mockMvc.perform(post("/admin/issuelinktypes/CreateIssueLinkType").param("name", "duplicate_name")
                                                                         .param("inward", "inward")
                                                                         .param("outward", "outward"))
               .andExpect(view().name("/admin/issuelinktypes/CreateIssueLinkType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueLinkType"));
        
        verify(issueService).saveIssueLinkType(any(IssueLinkType.class));
    }
    
    @Test
    public void testEditIssueLinkTypeGet_allOK () throws Exception {
    
        when(issueService.getIssueLinkTypeById(anyInt())).thenReturn(new IssueLinkType());
        
        mockMvc.perform(get("/admin/issuelinktypes/EditIssueLinkType").param("id", "0"))
               .andExpect(view().name("/admin/issuelinktypes/EditIssueLinkType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueLinkType"));
        
        verify(issueService).getIssueLinkTypeById(anyInt());
    }
    
    @Test
    public void testEditIssueLinkTypePost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/issuelinktypes/EditIssueLinkType").param("id", "0")
                                                                       .param("name", "not_empty")
                                                                       .param("inward", "inward")
                                                                       .param("outward", "outward"))
               .andExpect(redirectedUrl("/admin/issuelinktypes/ViewIssueLinkTypes"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueLinkType"));
        
        verify(issueService).saveIssueLinkType(any(IssueLinkType.class));
    }
    
    @Test
    public void testEditIssueLinkTypePost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/issuelinktypes/EditIssueLinkType").param("id", "0").param("name", ""))
               .andExpect(view().name("/admin/issuelinktypes/EditIssueLinkType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueLinkType"));
        
        verify(issueService, never()).saveIssueLinkType(any(IssueLinkType.class));
    }
    
    @Test
    public void testEditIssueLinkTypePost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveIssueLinkType(any(IssueLinkType.class));
        
        mockMvc.perform(post("/admin/issuelinktypes/EditIssueLinkType").param("id", "0")
                                                                       .param("name", "duplicate_name")
                                                                       .param("inward", "inward")
                                                                       .param("outward", "outward"))
               .andExpect(view().name("/admin/issuelinktypes/EditIssueLinkType"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("issueLinkType"));
        
        verify(issueService).saveIssueLinkType(any(IssueLinkType.class));
    }
    
    @Test
    public void testDeleteIssueLinkType_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/issuelinktypes/DeleteIssueLinkType").param("id", "0"))
               .andExpect(redirectedUrl("/admin/issuelinktypes/ViewIssueLinkTypes"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(issueService).getIssueLinkTypeById(anyInt());
        verify(issueService).deleteIssueLinkType(any(IssueLinkType.class));
    }
    
    @Test
    public void testDeleteIssueLinkType_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(issueService).deleteIssueLinkType(any(IssueLinkType.class));
        
        mockMvc.perform(get("/admin/issuelinktypes/DeleteIssueLinkType").param("id", "0"))
               .andExpect(redirectedUrl("/admin/issuelinktypes/ViewIssueLinkTypes"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getIssueLinkTypeById(anyInt());
        verify(issueService).deleteIssueLinkType(any(IssueLinkType.class));
    }
    
    @Test
    public void testDeleteIssueLinkType_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(new DataIntegrityViolationException("")).when(issueService)
                                                        .deleteIssueLinkType(any(IssueLinkType.class));
        
        mockMvc.perform(get("/admin/issuelinktypes/DeleteIssueLinkType").param("id", "0"))
               .andExpect(redirectedUrl("/admin/issuelinktypes/ViewIssueLinkTypes"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getIssueLinkTypeById(anyInt());
        verify(issueService).deleteIssueLinkType(any(IssueLinkType.class));
    }
}
