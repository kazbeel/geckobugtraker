package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.service.IssueService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class EnvironmentControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @InjectMocks
    private EnvironmentController environmentController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(environmentController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewEnvironments () throws Exception {
    
        mockMvc.perform(get("/admin/environments/ViewEnvironments"))
               .andExpect(view().name("/admin/environments/ViewEnvironments"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("environments"));
        
        verify(issueService).getAllEnvironments();
    }
    
    @Test
    public void testCreateEnvironmentGet () throws Exception {
    
        mockMvc.perform(get("/admin/environments/CreateEnvironment"))
               .andExpect(view().name("/admin/environments/CreateEnvironment"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("environment"));
    }
    
    @Test
    public void testCreateEnvironmentPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/environments/CreateEnvironment").param("name", "env_name"))
               .andExpect(redirectedUrl("/admin/environments/ViewEnvironments"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("environment"));
        
        verify(issueService).saveEnvironment(any(Environment.class));
    }
    
    @Test
    public void testCreateEnvironmentPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/environments/CreateEnvironment").param("name", ""))
               .andExpect(view().name("/admin/environments/CreateEnvironment"))
               .andExpect(model().hasErrors())
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("environment"));
        
        verify(issueService, never()).saveEnvironment(any(Environment.class));
    }
    
    @Test
    public void testCreateEnvironmentPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveEnvironment(any(Environment.class));
        
        mockMvc.perform(post("/admin/environments/CreateEnvironment").param("name", "env_name"))
               .andExpect(view().name("/admin/environments/CreateEnvironment"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("environment"));
        
        verify(issueService).saveEnvironment(any(Environment.class));
    }
    
    @Test
    public void testEditEnvironmentGet_allOK () throws Exception {
    
        when(issueService.getEnvironmentById(anyInt())).thenReturn(new Environment());
        
        mockMvc.perform(get("/admin/environments/EditEnvironment").param("id", "0"))
               .andExpect(view().name("/admin/environments/EditEnvironment"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("environment"));
        
        verify(issueService).getEnvironmentById(0);
    }
    
    @Test
    public void testEditEnvironmentGet_environmentDoesNotExist () throws Exception {
    
        when(issueService.getEnvironmentById(anyInt())).thenReturn(null);
        
        mockMvc.perform(get("/admin/environments/EditEnvironment").param("id", "0"))
               .andExpect(redirectedUrl("/admin/environments/ViewEnvironments"))
               .andExpect(model().size(0));
        
        verify(issueService).getEnvironmentById(anyInt());
    }
    
    @Test
    public void testEditEnvironmentPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/environments/EditEnvironment").param("id", "0").param("name", "env_name"))
               .andExpect(redirectedUrl("/admin/environments/ViewEnvironments"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("environment"));
        
        verify(issueService).saveEnvironment(any(Environment.class));
    }
    
    @Test
    public void testEditEnvironmentPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/environments/EditEnvironment").param("id", "0").param("name", ""))
               .andExpect(view().name("/admin/environments/EditEnvironment"))
               .andExpect(model().hasErrors())
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("environment"));
        
        verify(issueService, never()).saveEnvironment(any(Environment.class));
    }
    
    @Test
    public void testEditEnvironmentPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveEnvironment(any(Environment.class));
        
        mockMvc.perform(post("/admin/environments/EditEnvironment").param("id", "0").param("name", "env_name"))
               .andExpect(view().name("/admin/environments/EditEnvironment"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("environment"));
        
        verify(issueService).saveEnvironment(any(Environment.class));
    }
    
    @Test
    public void testDeleteEnvironment_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/environments/DeleteEnvironment").param("id", "0"))
               .andExpect(redirectedUrl("/admin/environments/ViewEnvironments"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(issueService).getEnvironmentById(anyInt());
        verify(issueService).deleteEnvironment(any(Environment.class));
    }
    
    @Test
    public void testDeleteEnvironment_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(issueService).deleteEnvironment(any(Environment.class));
        
        mockMvc.perform(get("/admin/environments/DeleteEnvironment").param("id", "0"))
               .andExpect(redirectedUrl("/admin/environments/ViewEnvironments"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getEnvironmentById(anyInt());
        verify(issueService).deleteEnvironment(any(Environment.class));
    }
    
    @Test
    public void testDeleteEnvironment_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(new DataIntegrityViolationException("")).when(issueService).deleteEnvironment(any(Environment.class));

        mockMvc.perform(get("/admin/environments/DeleteEnvironment").param("id", "0"))
               .andExpect(redirectedUrl("/admin/environments/ViewEnvironments"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getEnvironmentById(anyInt());
        verify(issueService).deleteEnvironment(any(Environment.class));
    }
}
