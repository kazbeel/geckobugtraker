package es.kazbeel.geckobugtracker.web.controller;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.builders.EnvironmentBuilder;
import es.kazbeel.geckobugtracker.builders.TagBuilder;
import es.kazbeel.geckobugtracker.model.Comment;
import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.secure.CurrentUserDetails;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class EditIssueControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @Mock
    private UserGroupService userGroupService;
    
    @InjectMocks
    private EditIssueController editIssueController;
    
    private MockMvc mockMvc;
    
    private User admin = new User();
    
    
    @Before
    public void setup () {
    
        CurrentUserDetails loggedUser = new CurrentUserDetails("admin",
                                                               "admin",
                                                               AuthorityUtils.createAuthorityList("ROLE_ADMIN"),
                                                               admin);
        Authentication auth = new UsernamePasswordAuthenticationToken(loggedUser, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(editIssueController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testEditIssueGet () throws Exception {
    
        when(issueService.getIssueById(anyInt())).thenReturn(new Issue());
        
        mockMvc.perform(get("/issues/0/edit"))
               .andExpect(view().name("/issues/EditIssue"))
               .andExpect(model().size(10))
               .andExpect(model().attributeExists("issue"))
               .andExpect(model().attributeExists("types"))
               .andExpect(model().attributeExists("statuses"))
               .andExpect(model().attributeExists("priorities"))
               .andExpect(model().attributeExists("resolutions"))
               .andExpect(model().attributeExists("users"))
               .andExpect(model().attributeExists("categories"))
               .andExpect(model().attributeExists("environments"))
               .andExpect(model().attributeExists("tags"))
               .andExpect(model().attributeExists("issuePackage"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).getAllEnvironments();
        verify(issueService).getAllTags();
        verify(issueService).getAllIssueTypes();
        verify(issueService).getAllStatuses();
        verify(issueService).getAllPriorities();
        verify(issueService).getAllResolutions();
        verify(userGroupService).getAllEnabledUsers();
        verify(issueService).getAllCategories();
        
        verifyNoMoreInteractions(issueService);
        verifyNoMoreInteractions(userGroupService);
    }
    
    @Test
    public void testEditIssuePost_withoutEnvironmentsAndTagsAndComment () throws Exception {
    
        Issue dummyIssue = new Issue();
        
        when(issueService.getIssueById(anyInt())).thenReturn(dummyIssue);
        
        mockMvc.perform(post("/issues/0/edit")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).saveIssue(dummyIssue);
        verify(issueService).getAllEnvironments();
        verify(issueService).getAllTags();
        
        verifyNoMoreInteractions(issueService);
        verifyNoMoreInteractions(userGroupService);
    }
    
    @Test
    public void testEditIssuePost_withComment () throws Exception {
    
        Issue dummyIssue = new Issue();
        
        when(issueService.getIssueById(anyInt())).thenReturn(dummyIssue);
        
        mockMvc.perform(post("/issues/0/edit").param("body", "...")
                                              .param("author.loginName", "login_name")
                                              .param("author.password", "password")
                                              .param("author.firstName", "first_name")
                                              .param("author.lastName", "last_name")
                                              .param("author.email", "email@email.com")
                                              .param("reporter.loginName", "login_name")
                                              .param("reporter.password", "password")
                                              .param("reporter.firstName", "first_name")
                                              .param("reporter.lastName", "last_name")
                                              .param("reporter.email", "email@email.com")
                                              .param("type.name", "type_name")
                                              .param("type.description", "type_description")
                                              .param("issue.summary", "summary"))
               .andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).saveIssue(dummyIssue);
        verify(issueService).getAllEnvironments();
        verify(issueService).getAllTags();
        verify(issueService).addCommentToIssue(eq(dummyIssue), eq(admin), any(Comment.class));
        
        verifyNoMoreInteractions(issueService);
        verifyNoMoreInteractions(userGroupService);
    }
    
    @Test
    public void testEditIssuePost_withExistingEnvironment () throws Exception {
    
        Issue dummyIssue = new Issue();
        List<Environment> dummyEnvList = new ArrayList<Environment>();
        dummyEnvList.add(EnvironmentBuilder.environment().withName("env1").build());
        
        when(issueService.getIssueById(anyInt())).thenReturn(dummyIssue);
        when(issueService.getAllEnvironments()).thenReturn(dummyEnvList);
        
        mockMvc.perform(post("/issues/0/edit").param("environmentStr", "env1")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).getAllEnvironments();
        verify(issueService).getAllTags();
        verify(issueService).saveIssue(dummyIssue);
        verify(issueService).getEnvironmentById(anyInt());
        
        verifyNoMoreInteractions(issueService);
        verifyNoMoreInteractions(userGroupService);
    }
    
    @Test
    public void testEditIssuePost_withNonExistingEnvironment () throws Exception {
    
        Issue dummyIssue = new Issue();
        
        when(issueService.getIssueById(anyInt())).thenReturn(dummyIssue);
        
        mockMvc.perform(post("/issues/0/edit").param("environmentStr", "env1")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).getAllEnvironments();
        verify(issueService).getAllTags();
        verify(issueService).saveIssue(dummyIssue);
        verify(issueService).saveEnvironment(any(Environment.class));
        
        verifyNoMoreInteractions(issueService);
        verifyNoMoreInteractions(userGroupService);
    }
    
    @Test
    public void testEditIssuePost_withExistingTag () throws Exception {
    
        Issue dummyIssue = new Issue();
        List<Tag> dummyTagList = new ArrayList<Tag>();
        dummyTagList.add(TagBuilder.tag().withName("tag1").build());
        
        when(issueService.getIssueById(anyInt())).thenReturn(dummyIssue);
        when(issueService.getAllTags()).thenReturn(dummyTagList);
        
        mockMvc.perform(post("/issues/0/edit").param("tagStr", "tag1")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).getAllEnvironments();
        verify(issueService).getAllTags();
        verify(issueService).saveIssue(dummyIssue);
        verify(issueService).getTagById(anyInt());
        
        verifyNoMoreInteractions(issueService);
        verifyNoMoreInteractions(userGroupService);
    }
    
    @Test
    public void testEditIssuePost_withNonExistingTag () throws Exception {
    
        Issue dummyIssue = new Issue();
        
        when(issueService.getIssueById(anyInt())).thenReturn(dummyIssue);
        
        mockMvc.perform(post("/issues/0/edit").param("tagStr", "tag1")).andExpect(redirectedUrl("/issues/0"));
        
        verify(issueService).getIssueById(0);
        verify(issueService).getAllEnvironments();
        verify(issueService).getAllTags();
        verify(issueService).saveIssue(dummyIssue);
        verify(issueService).saveTag(any(Tag.class));
        
        verifyNoMoreInteractions(issueService);
        verifyNoMoreInteractions(userGroupService);
    }
}
