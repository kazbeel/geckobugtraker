package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.service.IssueService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class ResolutionControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @InjectMocks
    private ResolutionController resolutionController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(resolutionController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewResolutions () throws Exception {
    
        mockMvc.perform(get("/admin/resolutions/ViewResolutions"))
               .andExpect(view().name("/admin/resolutions/ViewResolutions"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("resolutions"));
        
        verify(issueService).getAllResolutions();
    }
    
    @Test
    public void testCreateResolutionGet () throws Exception {
    
        mockMvc.perform(get("/admin/resolutions/CreateResolution"))
               .andExpect(view().name("/admin/resolutions/CreateResolution"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("resolution"));
    }
    
    @Test
    public void testCreateResolutionPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/resolutions/CreateResolution").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/resolutions/ViewResolutions"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("resolution"));
        
        verify(issueService).saveResolution(any(Resolution.class));
    }
    
    @Test
    public void testCreateResolutionPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/resolutions/CreateResolution").param("name", ""))
               .andExpect(view().name("/admin/resolutions/CreateResolution"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("resolution"));
        
        verify(issueService, never()).saveResolution(any(Resolution.class));
    }
    
    @Test
    public void testCreateResolutionPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveResolution(any(Resolution.class));
        
        mockMvc.perform(post("/admin/resolutions/CreateResolution").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/resolutions/CreateResolution"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("resolution"));
        
        verify(issueService).saveResolution(any(Resolution.class));
    }
    
    @Test
    public void testEditResolutionGet_allOK () throws Exception {
    
        when(issueService.getResolutionById(anyInt())).thenReturn(new Resolution());
        
        mockMvc.perform(get("/admin/resolutions/EditResolution").param("id", "0"))
               .andExpect(view().name("/admin/resolutions/EditResolution"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("resolution"));
        
        verify(issueService).getResolutionById(anyInt());
    }
    
    @Test
    public void testEditResolutionPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/resolutions/EditResolution").param("id", "0").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/resolutions/ViewResolutions"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("resolution"));
        
        verify(issueService).saveResolution(any(Resolution.class));
    }
    
    @Test
    public void testEditResolutionPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/resolutions/EditResolution").param("id", "0").param("name", ""))
               .andExpect(view().name("/admin/resolutions/EditResolution"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("resolution"));
        
        verify(issueService, never()).saveResolution(any(Resolution.class));
    }
    
    @Test
    public void testEditResolutionPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveResolution(any(Resolution.class));
        
        mockMvc.perform(post("/admin/resolutions/EditResolution").param("id", "0").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/resolutions/EditResolution"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("resolution"));
        
        verify(issueService).saveResolution(any(Resolution.class));
    }
    
    @Test
    public void testDeleteResolution_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/resolutions/DeleteResolution").param("id", "0"))
               .andExpect(redirectedUrl("/admin/resolutions/ViewResolutions"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(issueService).getResolutionById(anyInt());
        verify(issueService).deleteResolution(any(Resolution.class));
    }
    
    @Test
    public void testDeleteResolution_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(issueService).deleteResolution(any(Resolution.class));
        
        mockMvc.perform(get("/admin/resolutions/DeleteResolution").param("id", "0"))
               .andExpect(redirectedUrl("/admin/resolutions/ViewResolutions"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getResolutionById(anyInt());
        verify(issueService).deleteResolution(any(Resolution.class));
    }
    
    @Test
    public void testDeleteResolution_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(new DataIntegrityViolationException("")).when(issueService)
                                                        .deleteResolution(any(Resolution.class));
        
        mockMvc.perform(get("/admin/resolutions/DeleteResolution").param("id", "0"))
               .andExpect(redirectedUrl("/admin/resolutions/ViewResolutions"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getResolutionById(anyInt());
        verify(issueService).deleteResolution(any(Resolution.class));
    }
    
    @Test
    public void testSetResolutionOrder_moveUp () throws Exception {
    
        when(issueService.getResolutionById(anyInt())).thenReturn(new Resolution());
        
        mockMvc.perform(get("/admin/resolutions/SortResolution").param("id", "0").param("move", "up"))
               .andExpect(redirectedUrl("/admin/resolutions/ViewResolutions"))
               .andExpect(flash().attributeCount(0));
        
        verify(issueService).getResolutionById(anyInt());
        verify(issueService).setHigherOrderToResolution(any(Resolution.class));
        verify(issueService, never()).setLowerOrderToResolution(any(Resolution.class));
    }

    @Test
    public void testSetResolutionOrder_moveDown () throws Exception {
    
        when(issueService.getResolutionById(anyInt())).thenReturn(new Resolution());

        mockMvc.perform(get("/admin/resolutions/SortResolution").param("id", "0").param("move", "down"))
               .andExpect(redirectedUrl("/admin/resolutions/ViewResolutions"))
               .andExpect(flash().attributeCount(0));
        
        verify(issueService).getResolutionById(anyInt());
        verify(issueService).setLowerOrderToResolution(any(Resolution.class));
        verify(issueService, never()).setHigherOrderToResolution(any(Resolution.class));
    }

    @Test
    public void testSetResolutionOrder_invalidId () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(issueService).setHigherOrderToResolution(any(Resolution.class));
        
        mockMvc.perform(get("/admin/resolutions/SortResolution").param("id", "0").param("move", "up"))
               .andExpect(redirectedUrl("/admin/resolutions/ViewResolutions"))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getResolutionById(anyInt());
        verify(issueService, never()).setLowerOrderToResolution(any(Resolution.class));
        verify(issueService, never()).setHigherOrderToResolution(any(Resolution.class));
    }

    @Test
    public void testSetResolutionOrder_invalidMoveAction () throws Exception {
    
        when(issueService.getResolutionById(anyInt())).thenReturn(new Resolution());
        
        mockMvc.perform(get("/admin/resolutions/SortResolution").param("id", "0").param("move", "invalid"))
               .andExpect(redirectedUrl("/admin/resolutions/ViewResolutions"))
               .andExpect(flash().attributeCount(0));
        
        verify(issueService).getResolutionById(anyInt());
        verify(issueService, never()).setLowerOrderToResolution(any(Resolution.class));
        verify(issueService, never()).setHigherOrderToResolution(any(Resolution.class));
    }
}
