package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.Status;
import es.kazbeel.geckobugtracker.service.IssueService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class StatusControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @InjectMocks
    private StatusController statusController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(statusController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewStatuses () throws Exception {
    
        mockMvc.perform(get("/admin/statuses/ViewStatuses"))
               .andExpect(view().name("/admin/statuses/ViewStatuses"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("statuses"));
        
        verify(issueService).getAllStatuses();
    }
    
    @Test
    public void testCreateStatusGet () throws Exception {
    
        mockMvc.perform(get("/admin/statuses/CreateStatus"))
               .andExpect(view().name("/admin/statuses/CreateStatus"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("status"));
    }
    
    @Test
    public void testCreateStatusPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/statuses/CreateStatus").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/statuses/ViewStatuses"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("status"));
        
        verify(issueService).saveStatus(any(Status.class));
    }
    
    @Test
    public void testCreateStatusPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/statuses/CreateStatus").param("name", ""))
               .andExpect(view().name("/admin/statuses/CreateStatus"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("status"));
        
        verify(issueService, never()).saveStatus(any(Status.class));
    }
    
    @Test
    public void testCreateStatusPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveStatus(any(Status.class));
        
        mockMvc.perform(post("/admin/statuses/CreateStatus").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/statuses/CreateStatus"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("status"));
        
        verify(issueService).saveStatus(any(Status.class));
    }
    
    @Test
    public void testEditStatusGet_allOK () throws Exception {
    
        when(issueService.getStatusById(anyInt())).thenReturn(new Status());
        
        mockMvc.perform(get("/admin/statuses/EditStatus").param("id", "0"))
               .andExpect(view().name("/admin/statuses/EditStatus"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("status"));
        
        verify(issueService).getStatusById(anyInt());
    }
    
    @Test
    public void testEditStatusPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/statuses/EditStatus").param("id", "0").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/statuses/ViewStatuses"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("status"));
        
        verify(issueService).saveStatus(any(Status.class));
    }
    
    @Test
    public void testEditStatusPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/statuses/EditStatus").param("id", "0").param("name", ""))
               .andExpect(view().name("/admin/statuses/EditStatus"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("status"));
        
        verify(issueService, never()).saveStatus(any(Status.class));
    }
    
    @Test
    public void testEditStatusPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveStatus(any(Status.class));
        
        mockMvc.perform(post("/admin/statuses/EditStatus").param("id", "0").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/statuses/EditStatus"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("status"));
        
        verify(issueService).saveStatus(any(Status.class));
    }
    
    @Test
    public void testDeleteStatus_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/statuses/DeleteStatus").param("id", "0"))
               .andExpect(redirectedUrl("/admin/statuses/ViewStatuses"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(issueService).getStatusById(anyInt());
        verify(issueService).deleteStatus(any(Status.class));
    }
    
    @Test
    public void testDeleteStatus_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(issueService).deleteStatus(any(Status.class));
        
        mockMvc.perform(get("/admin/statuses/DeleteStatus").param("id", "0"))
               .andExpect(redirectedUrl("/admin/statuses/ViewStatuses"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getStatusById(anyInt());
        verify(issueService).deleteStatus(any(Status.class));
    }
    
    @Test
    public void testDeleteStatus_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(new DataIntegrityViolationException("")).when(issueService).deleteStatus(any(Status.class));
        
        mockMvc.perform(get("/admin/statuses/DeleteStatus").param("id", "0"))
               .andExpect(redirectedUrl("/admin/statuses/ViewStatuses"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getStatusById(anyInt());
        verify(issueService).deleteStatus(any(Status.class));
    }
}
