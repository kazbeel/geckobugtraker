package es.kazbeel.geckobugtracker.web.secure;


import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import es.kazbeel.geckobugtracker.builders.LoginInfoBuilder;
import es.kazbeel.geckobugtracker.model.LoginInfo;
import es.kazbeel.geckobugtracker.service.AuthService;


@RunWith(MockitoJUnitRunner.class)
public class LimitLogginAttemptsAuthenticationProviderTest {
    
    @Mock
    private AuthService authService;
    
    @Mock
    private UserDetailsService userDetailsService;
    
    @Mock
    private BCryptPasswordEncoder passwordEncoder;
    
    @InjectMocks
    private LimitLogginAttemptsAuthenticationProvider authenticationProvider;
    
    private static final List<GrantedAuthority> DUMMY_ROLES = AuthorityUtils.createAuthorityList("ROLE_DUMMY");
    
    private User dummyUser = new User("username", "password", DUMMY_ROLES);
    
    private User disabledUser = new User("username", "password", false, true, true, true, DUMMY_ROLES);
    
    private User lockedUser = new User("username", "password", true, true, true, false, DUMMY_ROLES);
    
    
    @Before
    public void setUp () {
    
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);
    }
    
    @Test
    public void testAuthenticate_allOK () {
    
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("username", "password");
        
        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(dummyUser);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        
        authenticationProvider.authenticate(token);
        
        verify(authService).resetFailAttemptsFromLoginInfo(anyString());
        verifyNoMoreInteractions(authService);
    }
    
    @Test(expected = BadCredentialsException.class)
    public void testAuthenticate_badCredentials () {
    
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("username", "password");
        
        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(dummyUser);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);
        
        authenticationProvider.authenticate(token);
        
        verify(authService).resetFailAttemptsFromLoginInfo(anyString());
        verifyNoMoreInteractions(authService);
    }
    
    @Test(expected = BadCredentialsException.class)
    public void testAuthenticate_userDoesNotExist () {
    
        // HideNotFoundExceptions is activated for security reasons. That way any malicious user will never know whether
        // the user is registered or not.
        
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("username", "password");
        
        when(userDetailsService.loadUserByUsername(anyString())).thenThrow(new UsernameNotFoundException(""));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);
        
        authenticationProvider.authenticate(token);
        
        verify(authService).resetFailAttemptsFromLoginInfo(anyString());
        verifyNoMoreInteractions(authService);
    }
    
    @Test(expected = DisabledException.class)
    public void testAuthenticate_userIsDisabled () {
    
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("username", "password");
        
        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(disabledUser);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        
        authenticationProvider.authenticate(token);
        
        verify(authService).resetFailAttemptsFromLoginInfo(anyString());
        verifyNoMoreInteractions(authService);
    }
    
    @Test(expected = LockedException.class)
    public void testAuthenticate_userIsLockedAndBanTimeHasNotExpiredYet () {
    
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("username", "password");
        
        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(lockedUser);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(authService.getLoginInfoByUsername(anyString())).thenReturn(new LoginInfo());
        
        authenticationProvider.authenticate(token);
        
        verify(authService).resetFailAttemptsFromLoginInfo(anyString());
        verify(authService).getLoginInfoByUsername("username");
    }
    
    @Test
    public void testAuthenticate_userIsLockedAndBanTimeHasExpiredAndWithGoodCredentials () {
    
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("username", "password");
        LoginInfo loginInfo = LoginInfoBuilder.loginInfo()
                                              .withUsername(token.getName())
                                              .withLocked(true)
                                              .withLastUpdateOn(new Date(123456))
                                              .build();
        
        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(lockedUser).thenReturn(dummyUser);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(authService.getLoginInfoByUsername(anyString())).thenReturn(loginInfo);
        
        authenticationProvider.authenticate(token);
        
        verify(authService, never()).resetFailAttemptsFromLoginInfo("username");
        verify(authService).getLoginInfoByUsername("username");
        verify(authService).saveLoginInfo(loginInfo);
    }
    
    @Test(expected = BadCredentialsException.class)
    public void testAuthenticate_userIsLockedAndBanTimeHasExpiredButWithBadRightCredentials () {
    
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("username", "password");
        LoginInfo loginInfo = LoginInfoBuilder.loginInfo()
                                              .withUsername("username")
                                              .withLocked(true)
                                              .withLastUpdateOn(new Date(123456))
                                              .build();
        
        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(lockedUser).thenReturn(dummyUser);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);
        when(authService.getLoginInfoByUsername(anyString())).thenReturn(loginInfo);
        
        authenticationProvider.authenticate(token);
        
        verify(authService, never()).resetFailAttemptsFromLoginInfo("username");
        verify(authService).getLoginInfoByUsername("username");
        verify(authService, never()).saveLoginInfo(loginInfo);
    }
}
