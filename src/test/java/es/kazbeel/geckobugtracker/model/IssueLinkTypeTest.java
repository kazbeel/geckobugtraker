package es.kazbeel.geckobugtracker.model;


import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.IssueLinkTypeBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class IssueLinkTypeTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLinkType issueLinkType = IssueLinkTypeBuilder.issueLinkType()
                                                          .withName(null)
                                                          .withInward("inward")
                                                          .withOutward("outward")
                                                          .build();
        
        session.save(issueLinkType);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLinkType issueLinkType = IssueLinkTypeBuilder.issueLinkType()
                                                          .withName(RandomStringUtils.randomAlphabetic(256))
                                                          .withInward("inward")
                                                          .withOutward("outward")
                                                          .build();
        
        session.save(issueLinkType);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @DatabaseSetup("IssueLinkTypeTest.xml")
    @Transactional
    public void testNameMustBeUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLinkType ilt1 = (IssueLinkType) session.get(IssueLinkType.class, 1);
        IssueLinkType ilt2 = (IssueLinkType) session.get(IssueLinkType.class, 2);
        
        ilt1.setName(ilt2.getName());
        
        session.save(ilt1);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testInwardCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLinkType issueLinkType = IssueLinkTypeBuilder.issueLinkType()
                                                          .withName("name")
                                                          .withInward(null)
                                                          .withOutward("outward")
                                                          .build();
        
        session.save(issueLinkType);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testInwardCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLinkType issueLinkType = IssueLinkTypeBuilder.issueLinkType()
                                                          .withName("name")
                                                          .withInward(RandomStringUtils.randomAlphabetic(256))
                                                          .withOutward("outward")
                                                          .build();
        
        session.save(issueLinkType);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testOutwardCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLinkType issueLinkType = IssueLinkTypeBuilder.issueLinkType()
                                                          .withName("name")
                                                          .withInward("inward")
                                                          .withOutward(null)
                                                          .build();
        
        session.save(issueLinkType);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testOutwardCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLinkType issueLinkType = IssueLinkTypeBuilder.issueLinkType()
                                                          .withName("name")
                                                          .withInward("inward")
                                                          .withOutward(RandomStringUtils.randomAlphabetic(256))
                                                          .build();
        
        session.save(issueLinkType);
        session.flush();
        session.clear();
    }
}
