package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.validation.ConstraintViolationException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.IssueBuilder;
import es.kazbeel.geckobugtracker.builders.IssueLinkBuilder;
import es.kazbeel.geckobugtracker.builders.IssueLinkTypeBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class IssueLinkTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("IssueLinkTest.xml")
    @Transactional
    public void testSourceCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue dest = (Issue) session.get(Issue.class, 1);
        IssueLinkType issueLinkType = (IssueLinkType) session.get(IssueLinkType.class, 1);
        
        IssueLink issueLink = IssueLinkBuilder.issueLink()
                                              .withSource(null)
                                              .withDestination(dest)
                                              .withLinkType(issueLinkType)
                                              .build();
        
        session.save(issueLink);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("IssueLinkTest.xml")
    @Transactional
    public void testDestinationCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue src = (Issue) session.get(Issue.class, 1);
        IssueLinkType issueLinkType = (IssueLinkType) session.get(IssueLinkType.class, 1);
        
        IssueLink issueLink = IssueLinkBuilder.issueLink()
                                              .withSource(src)
                                              .withDestination(null)
                                              .withLinkType(issueLinkType)
                                              .build();
        
        session.save(issueLink);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("IssueLinkTest.xml")
    @Transactional
    public void testLinkTypeCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue src = (Issue) session.get(Issue.class, 1);
        Issue dest = (Issue) session.get(Issue.class, 2);
        
        IssueLink issueLink = IssueLinkBuilder.issueLink()
                                              .withSource(src)
                                              .withDestination(dest)
                                              .withLinkType(null)
                                              .build();
        
        session.save(issueLink);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("IssueLinkTest.xml")
    @Transactional
    public void testGoodMappingWithSource () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLink issueLink = (IssueLink) session.get(IssueLink.class, 1);
        
        assertThat(issueLink, is(not(nullValue())));
        assertThat(issueLink.getSource(), is(IssueBuilder.issue().withId(1).build()));
    }
    
    @Test
    @DatabaseSetup("IssueLinkTest.xml")
    @Transactional
    public void testGoodMappingWithDestination () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLink issueLink = (IssueLink) session.get(IssueLink.class, 1);
        
        assertThat(issueLink, is(not(nullValue())));
        assertThat(issueLink.getDestination(), is(IssueBuilder.issue().withId(2).build()));
    }
    
    @Test
    @DatabaseSetup("IssueLinkTest.xml")
    @Transactional
    public void testGoodMappingWithIssueLinkType () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueLink issueLink = (IssueLink) session.get(IssueLink.class, 1);
        
        assertThat(issueLink, is(not(nullValue())));
        assertThat(issueLink.getLinkType(), is(IssueLinkTypeBuilder.issueLinkType()
                                                                   .withId(1)
                                                                   .withName("name_1")
                                                                   .build()));
    }
}
