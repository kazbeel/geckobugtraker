package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.SectionBuilder;
import es.kazbeel.geckobugtracker.builders.UserBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class ProjectTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 1L);
        project.setName(null);
        
        session.save(project);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 1L);
        project.setName(RandomStringUtils.randomAlphabetic(256));
        
        session.save(project);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testNameMustBeUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project1 = (Project) session.get(Project.class, 1L);
        Project project2 = (Project) session.get(Project.class, 2L);
        project1.setName(project2.getName());
        
        session.save(project1);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testDescriptionCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 1L);
        project.setDescription(RandomStringUtils.randomAlphabetic(4097));
        
        session.save(project);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testSectionCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 1L);
        project.setSection(null);
        
        session.save(project);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testProjectWithSection () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 1L);
        
        session.save(project);
        session.flush();
        session.clear();
        
        assertThat(project, is(not(nullValue())));
        assertThat(project.getSection(), is(not(nullValue())));
        assertThat(project.getSection(), is(SectionBuilder.section().withId(1L).withName("section_name_1").build()));
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testDevLeaderCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 1L);
        project.setDevLeader(null);
        
        session.save(project);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testProjectWithDevLeader () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 1L);
        
        session.save(project);
        session.flush();
        session.clear();
        
        assertThat(project, is(not(nullValue())));
        assertThat(project.getDevLeader(), is(not(nullValue())));
        assertThat(project.getDevLeader(),
                   is(UserBuilder.user()
                                 .withId(1)
                                 .withLoginName("login_name_1")
                                 .withPassword("passwd")
                                 .withFirstName("first_name")
                                 .withLastName("last_name")
                                 .withEmail("e_mail_1")
                                 .build()));
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testQaLeaderCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 1L);
        project.setQaLeader(null);
        
        session.save(project);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testProjectWithQaLeader () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 1L);
        
        session.save(project);
        session.flush();
        session.clear();
        
        assertThat(project, is(not(nullValue())));
        assertThat(project.getQaLeader(), is(not(nullValue())));
        assertThat(project.getQaLeader(),
                   is(UserBuilder.user()
                                 .withId(1)
                                 .withLoginName("login_name_1")
                                 .withPassword("passwd")
                                 .withFirstName("first_name")
                                 .withLastName("last_name")
                                 .withEmail("e_mail_1")
                                 .build()));
    }
    
    @Test
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testDeleteProject_withOwnComponents () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 4L);
        Component component = (Component) session.get(Component.class, 1L);
        
        assertThat(project, is(not(nullValue())));
        assertThat(project.getChildren(), hasSize(1));
        assertThat(component, is(not(nullValue())));
        
        session.delete(project);
        session.flush();
        session.clear();
        
        component = (Component) session.get(Component.class, 1L);
        assertThat(component, is(nullValue()));
    }
    
    @Test
    @DatabaseSetup("ProjectTest.xml")
    @Transactional
    public void testDeleteProject_withVersionsInCascade () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Project project = (Project) session.get(Project.class, 3L);
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        
        assertThat(project, is(not(nullValue())));
        assertThat(project.getVersions(), hasSize(1));
        assertThat(version, is(not(nullValue())));
        
        session.delete(project);
        session.flush();
        session.clear();
        
        version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        assertThat(version, is(nullValue()));
    }
}
