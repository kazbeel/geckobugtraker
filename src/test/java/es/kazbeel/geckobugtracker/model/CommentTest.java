package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.CommentBuilder;
import es.kazbeel.geckobugtracker.builders.IssueBuilder;
import es.kazbeel.geckobugtracker.builders.UserBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class CommentTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("CommentTest.xml")
    @Transactional
    public void testIssueCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        User author = (User) session.get(User.class, 1);
        
        Comment comment = CommentBuilder.comment()
                                        .withId(1)
                                        .withIssue(null)
                                        .withAuthor(author)
                                        .withBody(null)
                                        .build();
        
        session.save(comment);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("CommentTest.xml")
    @Transactional
    public void testGoodMappingWithIssue () {
    
        Comment comment = (Comment) sessionFactory.getCurrentSession().get(Comment.class, 1);
        
        assertThat(comment, is(not(nullValue())));
        assertThat(comment.getIssue(), is(IssueBuilder.issue().withId(1).build()));
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("CommentTest.xml")
    @Transactional
    public void testAuthorCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        
        Comment comment = CommentBuilder.comment()
                                        .withId(1)
                                        .withIssue(issue)
                                        .withAuthor(null)
                                        .withBody(null)
                                        .build();
        
        session.save(comment);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("CommentTest.xml")
    @Transactional
    public void testGoodMappingWithAuthor () {
    
        Comment comment = (Comment) sessionFactory.getCurrentSession().get(Comment.class, 1);
        
        assertThat(comment, is(not(nullValue())));
        assertThat(comment.getAuthor(), is(UserBuilder.user().withId(1).withLoginName("login_name_author").build()));
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("CommentTest.xml")
    @Transactional
    public void testBodyCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        User author = (User) session.get(User.class, 1);
        
        Comment comment = CommentBuilder.comment()
                                        .withId(1)
                                        .withIssue(issue)
                                        .withAuthor(author)
                                        .withBody(null)
                                        .build();
        
        session.save(comment);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("CommentTest.xml")
    @Transactional
    public void testBodyCannotBelonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        User author = (User) session.get(User.class, 1);
        
        Comment comment = CommentBuilder.comment()
                                        .withId(1)
                                        .withIssue(issue)
                                        .withAuthor(author)
                                        .withBody(RandomStringUtils.randomAlphabetic(100001))
                                        .build();
        
        session.save(comment);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("CommentTest.xml")
    @Transactional
    public void testRelationWithParent () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Comment comment = (Comment) session.get(Comment.class, 1);
        
        assertThat(comment, is(not(nullValue())));
        assertThat(comment.getParent(), is(nullValue()));
        
        comment = (Comment) session.get(Comment.class, 3);
        
        assertThat(comment, is(not(nullValue())));
        assertThat(comment.getParent(), is(CommentBuilder.comment().withId(2).build()));
        
        comment = (Comment) session.get(Comment.class, 5);
        
        assertThat(comment, is(not(nullValue())));
        assertThat(comment.getParent(), is(CommentBuilder.comment().withId(3).build()));
    }
    
    @Test
    @DatabaseSetup("CommentTest.xml")
    @Transactional
    public void testRelationWithChildren () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Comment comment = (Comment) session.get(Comment.class, 1);
        
        assertThat(comment, is(not(nullValue())));
        assertThat(comment.getChildren(), hasSize(0));
        
        comment = (Comment) session.get(Comment.class, 2);
        
        assertThat(comment, is(not(nullValue())));
        assertThat(comment.getChildren(), hasSize(3));
        
        comment = (Comment) session.get(Comment.class, 3);
        
        assertThat(comment, is(not(nullValue())));
        assertThat(comment.getChildren(), hasSize(1));
    }
    
    @Test
    @DatabaseSetup("CommentTest.xml")
    @Transactional
    public void testCascadeActionWhenDeletingParent () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Comment comment = (Comment) session.get(Comment.class, 2);
        
        assertThat(comment, is(not(nullValue())));
        assertThat(comment.getChildren(), hasSize(3));
        assertThat(comment.getChildren(),
                   contains(CommentBuilder.comment().withId(3).build(),
                            CommentBuilder.comment().withId(4).build(),
                            CommentBuilder.comment().withId(6).build()));
        
        session.delete(comment);
        session.flush();
        session.clear();
        
        Comment child = (Comment) session.get(Comment.class, 3);
        assertThat(child, is(nullValue()));
        
        child = (Comment) session.get(Comment.class, 4);
        assertThat(child, is(nullValue()));
        
        child = (Comment) session.get(Comment.class, 6);
        assertThat(child, is(nullValue()));
    }
}
