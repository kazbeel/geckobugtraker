package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.SectionBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class SectionTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeNull () {
    
        Section section = SectionBuilder.section()
                                        .withName(null)
                                        .withDescription("description")
                                        .withSortValue(0)
                                        .build();
        
        Session session = sessionFactory.getCurrentSession();
        
        session.save(section);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeLonger () {
    
        Section section = SectionBuilder.section()
                                        .withName(RandomStringUtils.randomAlphabetic(256))
                                        .withDescription("description")
                                        .withSortValue(0)
                                        .build();
        
        Session session = sessionFactory.getCurrentSession();
        
        session.save(section);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @DatabaseSetup("SectionTest.xml")
    @Transactional
    public void testNameMustBeUnique () {
    
        Session session = sessionFactory.getCurrentSession();

        Section section1 = (Section) session.get(Section.class, 1L);
        Section section2 = (Section) session.get(Section.class, 2L);
        section1.setName(section2.getName());
        
        session.save(section1);
        session.flush();
        session.clear();
    }

    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testDescriptionCannotBeLonger () {
    
        Section section = SectionBuilder.section()
                                        .withName("name")
                                        .withDescription(RandomStringUtils.randomAlphabetic(4097))
                                        .withSortValue(0)
                                        .build();
        
        Session session = sessionFactory.getCurrentSession();
        
        session.save(section);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testSortValueCannotBeNull () {
    
        Section section = SectionBuilder.section()
                                        .withName("name")
                                        .withDescription("description")
                                        .withSortValue(null)
                                        .build();
        
        Session session = sessionFactory.getCurrentSession();
        
        session.save(section);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testSortValueCannotBeNegative () {
    
        Section section = SectionBuilder.section()
                                        .withName("name")
                                        .withDescription("description")
                                        .withSortValue(-1)
                                        .build();
        
        Session session = sessionFactory.getCurrentSession();
        
        session.save(section);
        session.flush();
        session.clear();
    }

    @Test
    @DatabaseSetup("SectionTest.xml")
    @Transactional
    public void testSectionWithProjects () {
        
        Section section = (Section) sessionFactory.getCurrentSession().get(Section.class, 1L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section.getProjects(), hasSize(2));
    }

    @Test
    @DatabaseSetup("SectionTest.xml")
    @Transactional
    public void testDeleteSection_withProjects () {
        
        Session session = sessionFactory.getCurrentSession();
        
        Section section = (Section) session.get(Section.class, 2L);
        Project project = (Project) session.get(Project.class, 2L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section.getProjects(), hasSize(1));
        assertThat(project, is(not(nullValue())));
        
        session.delete(section);
        session.flush();
        session.clear();
        
        project = (Project) session.get(Project.class, 2L);
        assertThat(project, is(nullValue()));
    }
}
