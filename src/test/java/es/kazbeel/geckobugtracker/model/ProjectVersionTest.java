package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.ProjectBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class ProjectVersionTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        version.setName(null);
        
        session.save(version);
        session.flush();
        session.clear();
    }

    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        version.setName(RandomStringUtils.randomAlphabetic(256));
        
        session.save(version);
        session.flush();
        session.clear();
    }

    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testNameMustBeUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version1 = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        ProjectVersion version2 = (ProjectVersion) session.get(ProjectVersion.class, 2L);
        version1.setName(version2.getName());
        
        session.save(version1);
        session.flush();
        session.clear();
    }

    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testDescriptionCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        version.setDescription(RandomStringUtils.randomAlphabetic(4097));
        
        session.save(version);
        session.flush();
        session.clear();
    }

    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testSortValueCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        version.setSortValue(null);
        
        session.save(version);
        session.flush();
        session.clear();
    }

    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testSortValueMustBePositive () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        version.setSortValue(-1);
        
        session.save(version);
        session.flush();
        session.clear();
    }

    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testReleasedCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        version.setReleased(null);
        
        session.save(version);
        session.flush();
        session.clear();
    }

    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testArchivedCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        version.setArchived(null);
        
        session.save(version);
        session.flush();
        session.clear();
    }

    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testProjectCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);
        version.setProject(null);
        
        session.save(version);
        session.flush();
        session.clear();
    }

    @Test
    @DatabaseSetup("ProjectVersionTest.xml")
    @Transactional
    public void testProjectVersionWithProject () {
    
        Session session = sessionFactory.getCurrentSession();
        
        ProjectVersion version = (ProjectVersion) session.get(ProjectVersion.class, 1L);

        assertThat(version, is(not(nullValue())));
        assertThat(version.getProject(), is(not(nullValue())));
        assertThat(version.getProject(), is(ProjectBuilder.project().withId(1L).withName("project_name_1").build()));
    }
}
