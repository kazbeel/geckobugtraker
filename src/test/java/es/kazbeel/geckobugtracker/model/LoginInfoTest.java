package es.kazbeel.geckobugtracker.model;


import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;

import es.kazbeel.geckobugtracker.builders.LoginInfoBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class LoginInfoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testUsernameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        LoginInfo loginInfo = LoginInfoBuilder.loginInfo()
                                              .withUsername(null)
                                              .withFailAttempts(0)
                                              .withLocked(false)
                                              .build();
        
        session.save(loginInfo);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testUsernameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        LoginInfo loginInfo = LoginInfoBuilder.loginInfo()
                                              .withUsername(RandomStringUtils.randomAlphabetic(256))
                                              .withFailAttempts(0)
                                              .withLocked(false)
                                              .build();
        
        session.save(loginInfo);
        session.flush();
        session.clear();
    }
}
