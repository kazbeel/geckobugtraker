<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Gecko - Issues Assigned To Me</title>

    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
 </head>

<body>
    
    <jsp:include page="../TopNavBar.jsp" />

    <h1>All Issues</h1>
    <div class="uk-overflow-container">
    
	    <c:if test="${not empty issues}">
            <ul style="margin-right: 10px;" class="uk-pagination uk-pagination-right">
                <c:choose>
                    <c:when test="${isFirst == false && not empty param.page}">
	                    <spring:url value="/issues/AssignedToMe" var="prevPage" htmlEscape="true">
	                       <spring:param name="page" value="${param.page - 1}" />
	                    </spring:url>
	                    <li><a href="${prevPage}"><i class="uk-icon-angle-left"></i></a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="uk-disabled"><span><i class="uk-icon-angle-left"></i></span></li>
                    </c:otherwise>
                </c:choose>
                
                <c:choose>
                    <c:when test="${empty param.page}">
                        <li>0 of ${totalPages}</li>
                    </c:when>
                    <c:otherwise>
                        <li>${param.page} of ${totalPages}</li>
                    </c:otherwise>
                </c:choose>
                
                <c:choose>
                    <c:when test="${isLast == false}">
                        <spring:url value="/issues/AssignedToMe" var="nextPage" htmlEscape="true">
                           <spring:param name="page" value="${param.page + 1}" />
                        </spring:url>
                        <li><a href="${nextPage}"><i class="uk-icon-angle-right"></i></a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="uk-disabled"><span><i class="uk-icon-angle-right"></i></span></li>
                    </c:otherwise>
                </c:choose>
            </ul>

	        <table class="uk-table uk-table-hover">
	           <thead>
	               <tr>
                        <th class="uk-text-center">ID</th>
                        <th class="uk-text-center">Type</th>
                        <th class="uk-text-center">Priority</th>
                        <th class="uk-text-left">Summary</th>
                        <th class="uk-text-center">Reporter</th>
                        <th class="uk-text-center">Assignee</th>
                        <th class="uk-text-center">Status</th>
                        <th class="uk-text-center">Created</th>
                        <th class="uk-text-center">Updated</th>
                </tr>
	           </thead>
	           
	           <tbody>
	                <c:forEach items="${issues}" var="issue">
	                   <spring:url value="/issues/{issueId}" var="issueUrl" htmlEscape="true">
	                       <spring:param name="issueId" value="${issue.id}" />
	                   </spring:url>
	                   <tr>
	                        <td class="uk-width-1-10 uk-text-center">
	                            <a href="${issueUrl}"><c:out value="${issue.id}" /></a>
	                        </td>
                            <td class="uk-width-1-10 uk-text-center"><c:out value="${issue.type.name}" /></td>
                            <td class="uk-width-1-10 uk-text-center"><c:out value="${issue.priority.name}" /></td>
                            <td class="uk-width-2-10 uk-text-left">
                                <a class="uk-link-muted" href="${issueUrl}"><c:out value="${issue.summary}" /></a>
                            </td>
                            <td class="uk-width-1-10 uk-text-center"><c:out value="${issue.reporter.loginName}" /></td>
                            <td class="uk-width-1-10 uk-text-center"><c:out value="${issue.assignee.loginName}" /></td>
                            <td class="uk-width-1-10 uk-text-center"><c:out value="${issue.status.name}" /></td>
                            <td class="uk-width-1-10 uk-text-center"><fmt:formatDate pattern="dd-MM-yyyy" value="${issue.createdOn}" /></td>
                            <td class="uk-width-1-10 uk-text-center"><fmt:formatDate pattern="dd-MM-yyyy" value="${issue.lastUpdateOn}" /></td>
	                   </tr>
	                </c:forEach>
	           </tbody>
	        </table>
	    </c:if>
    </div>
        
</body>
</html>