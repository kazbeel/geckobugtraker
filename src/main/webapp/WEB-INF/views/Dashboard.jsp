<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Gecko - Dashboard</title>

    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
 </head>

<body>
    <jsp:include flush="true" page="TopNavBar.jsp" />
    
    <div class="uk-container uk-container-center uk-margin-large">
	    <c:if test="${not empty formatError}">
	        <div class="uk-alert uk-alert-danger" data-uk-alert>
	            <a href="" class="uk-alert-close uk-close"></a>
	            <p>${formatError}</p>
	        </div>
	    </c:if>

        <c:if test="${not empty notFound}">
            <div class="uk-alert uk-alert-warning" data-uk-alert>
                <a href="" class="uk-alert-close uk-close"></a>
                <p>${notFound}</p>
            </div>
        </c:if>

        <div class="uk-grid">
            
            <!-- Left Column -->
            <div class="uk-width-1-2">

                <div class="uk-panel uk-panel-box">
                    <div class="uk-panel-title">Assigned to Me (Unresolved)</div>
                    
                    <c:choose>
                        <c:when test="${not empty issuesAssignedToMe}">
	                    <table class="uk-table uk-table-condensed uk-text-small">
	                        <thead>
	                            <tr>
	                                <th class="uk-text-center">ID</th>
	                                <th class="uk-text-center">Prio</th>
	                                <th class="uk-text-left">Summary</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <c:forEach items="${issuesAssignedToMe}" var="issue">
	                                <spring:url value="/issues/{issueId}" var="issueUrl" htmlEscape="true">
	                                   <spring:param name="issueId" value="${issue.id}" />
	                                </spring:url>
	                                
	                                <tr>
	                                    <td class="uk-width-1-10 uk-text-center">
	                                        <a href="${issueUrl}"><c:out value="${issue.id}" /></a>
	                                    </td>
	                                    <td class="uk-width-1-10 uk-text-center">
	                                       <c:out value="${issue.priority.name}" />
	                                    </td>
	                                    <td class="uk-width-8-10 uk-text-left">
	                                       <a class="uk-link-muted" href="${issueUrl}">
	                                       <c:out value="${issue.summary}" />
	                                       </a>
	                                    </td>
	                                </tr>
	                            </c:forEach>
	                        </tbody>
	                    </table>
	                    
	                    <table class="uk-width-1-1">
                            <tr>
                                <td class="uk-text-left" >
                                    <a class="uk-text-small uk-text-muted" href="<c:url value="/issues/AssignedToMe" />">See more</a>
                                </td>
                                <td class="uk-text-right">
                                    <span class="uk-text-small uk-text-muted"> Total: ${totalAssignedToMe}</span>
                                </td>
                            </tr>
	                    </table>
                        </c:when>
                        <c:otherwise>
                            <p class="uk-text-small uk-text-left"> There are no issues assigned to you.</p>
                        </c:otherwise>
                    </c:choose>
                </div>

                <div class="uk-panel uk-panel-box">
                    <div class="uk-panel-title">Watched</div>

                    <c:choose>
                        <c:when test="${not empty issuesWatchedByMe}">
		                    <table class="uk-table uk-table-condensed uk-text-small">
		                        <thead>
		                            <tr>
		                                <th class="uk-text-center">ID</th>
		                                <th class="uk-text-center">Prio</th>
		                                <th class="uk-text-left">Summary</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <c:forEach items="${issuesWatchedByMe}" var="issue">
		                                <spring:url value="/issues/{issueId}" var="issueUrl" htmlEscape="true">
		                                    <spring:param name="issueId" value="${issue.id}" />
		                                </spring:url>
		                                
		                                <tr>
		                                    <td class="uk-width-1-10 uk-text-center">
		                                        <a href="${issueUrl}"><c:out value="${issue.id}" /></a>
		                                    </td>
		                                    <td class="uk-width-1-10 uk-text-center">
		                                       <c:out value="${issue.priority.name}" />
		                                    </td>
		                                    <td class="uk-width-8-10 uk-text-left">
		                                       <a class="uk-link-muted" href="${issueUrl}">
		                                       <c:out value="${issue.summary}" />
		                                       </a>
		                                    </td>
		                                </tr>
		                            </c:forEach>
		                        </tbody>
		                    </table>
                    
	                        <table class="uk-width-1-1">
	                            <tr>
	                                <td class="uk-text-left" >
	                                    <a class="uk-text-small uk-text-muted" href="<c:url value="/issues/WatchedByMe" />">See more</a>
	                                </td>
	                                <td class="uk-text-right">
	                                    <span class="uk-text-small uk-text-muted"> Total: ${totalWatched}</span>
	                                </td>
	                            </tr>
	                        </table>
                        </c:when>
                        
                        <c:otherwise>
                            <p class="uk-text-small uk-text-left">You are not watching any issue.</p>
                        </c:otherwise>
                    </c:choose>
                </div>

                <div class="uk-panel uk-panel-box">
                    <div class="uk-panel-title">Voted</div>
                    
                    <c:choose>
                        <c:when test="${not empty issuesVotedByMe}">
		                    <table class="uk-table uk-table-condensed uk-text-small">
		                        <thead>
		                            <tr>
		                                <th class="uk-text-center">ID</th>
		                                <th class="uk-text-center">Prio</th>
		                                <th class="uk-text-left">Summary</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <c:forEach items="${issuesVotedByMe}" var="issue">
		                                <spring:url value="/issues/{issueId}" var="issueUrl" htmlEscape="true">
		                                   <spring:param name="issueId" value="${issue.id}" />
		                                </spring:url>
		                                
		                                <tr>
		                                    <td class="uk-width-1-10 uk-text-center">
		                                        <a href="${issueUrl}"><c:out value="${issue.id}" /></a>
		                                    </td>
		                                    <td class="uk-width-1-10 uk-text-center">
		                                       <c:out value="${issue.priority.name}" />
		                                    </td>
		                                    <td class="uk-width-8-10 uk-text-left">
		                                       <a class="uk-link-muted" href="${issueUrl}">
		                                       <c:out value="${issue.summary}" />
		                                       </a>
		                                    </td>
		                                </tr>
		                            </c:forEach>
		                        </tbody>
		                    </table>
		                    
                            <table class="uk-width-1-1">
                                <tr>
                                    <td class="uk-text-left" >
                                        <a class="uk-text-small uk-text-muted" href="<c:url value="/issues/VotedByMe" />">See more</a>
                                    </td>
                                    <td class="uk-text-right">
                                        <span class="uk-text-small uk-text-muted"> Total: ${totalVoted}</span>
                                    </td>
                                </tr>
                            </table>
                        </c:when>
                        
                        <c:otherwise>
                            <p class="uk-text-small uk-text-left">You are not voting for any issue.</p>
                        </c:otherwise>
                    </c:choose>
                </div>

            </div>

            <!-- Right Column -->
            <div class="uk-width-1-2">

                <div class="uk-panel uk-panel-box">
                    <div class="uk-panel-title">Reported by Me</div>
                    
                    <c:choose>
                        <c:when test="${not empty issuesReportedByMe}">
		                    <table class="uk-table uk-table-condensed uk-text-small">
		                        <thead>
		                            <tr>
		                                <th class="uk-text-center">ID</th>
		                                <th class="uk-text-center">Prio</th>
		                                <th class="uk-text-left">Summary</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <c:forEach items="${issuesReportedByMe}" var="issue">
		                                <spring:url value="/issues/{issueId}" var="issueUrl" htmlEscape="true">
		                                   <spring:param name="issueId" value="${issue.id}" />
		                                </spring:url>
		                                
		                                <tr>
		                                    <td class="uk-width-1-10 uk-text-center">
		                                        <a href="${issueUrl}"><c:out value="${issue.id}" /></a>
		                                    </td>
		                                    <td class="uk-width-1-10 uk-text-center">
		                                       <c:out value="${issue.priority.name}" />
		                                    </td>
		                                    <td class="uk-width-8-10 uk-text-left">
		                                       <a class="uk-link-muted" href="${issueUrl}">
		                                       <c:out value="${issue.summary}" />
		                                       </a>
		                                    </td>
		                                </tr>
		                            </c:forEach>
		                        </tbody>
		                    </table>
		                    
                            <table class="uk-width-1-1">
                                <tr>
                                    <td class="uk-text-left" >
                                        <a class="uk-text-small uk-text-muted" href="<c:url value="/issues/ReportedByMe" />">See more</a>
                                    </td>
                                    <td class="uk-text-right">
                                        <span class="uk-text-small uk-text-muted"> Total: ${totalReportedByMe}</span>
                                    </td>
                                </tr>
                            </table>
                        </c:when>
                        
                        <c:otherwise>
                            <p class="uk-text-small uk-text-left">You have not reported any issue.</p>
                        </c:otherwise>
                    </c:choose>
                </div>

            </div>

        </div>
    </div>
</body>

</html>
