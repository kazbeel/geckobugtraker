<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Component Details</title>
    
    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/docs.css" />" type="text/css" />
</head>

<body>

<jsp:include page="../../TopNavBar.jsp" />


<div class="tm-middle">
	<div class="uk-container uk-container-center">
	
	    <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
            <div class="tm-sidebar uk-width-medium-1-4">
                <ul class="tm-nav uk-nav uk-nav-side" data-uk-nav>
                    <li><a href="<c:url value="/admin/users/ViewUsers" />">Users</a></li>
                    <li><a href="<c:url value="/admin/GroupsList" />">Groups</a></li>
                    <li><a href="<c:url value="/admin/IssueTypeList" />">Issue Types</a></li>
                    <li><a href="<c:url value="/admin/PriorityList" />">Priorities</a></li>
                    <li class="uk-active"><a href="<c:url value="/admin/resolutions/ViewResolutions" />">Resolutions</a></li>
                </ul>
            </div>
	        
            <spring:url value="/admin/components/DeleteComponent" var="delComponentUrl" htmlEscape="true">
                <spring:param name="id" value="${component.id}" />
            </spring:url>
            <spring:url value="/admin/components/EditComponent" var="editComponentUrl" htmlEscape="true">
                <spring:param name="id" value="${component.id}" />
            </spring:url>
            <spring:url value="/admin/components/ShareComponent" var="shareComponentUrl" htmlEscape="true">
                <spring:param name="id" value="${component.id}" />
            </spring:url>
            <spring:url value="/admin/projects/DetailsProject" var="rootProjectUrl" htmlEscape="true">
                <spring:param name="id" value="${component.rootProject.id}" />
            </spring:url>

	        <div class="tm-main uk-width-medium-3-4">
	            <c:if test="${not empty msgSuccess}">
	                <div class="uk-alert uk-alert-success">
	                   <a href="" class="uk-alert-close uk-close"></a>
	                   <p>${msgSuccess}</p>
	                </div>
	            </c:if>
	            <c:if test="${not empty msgError}">
	                <div class="uk-alert uk-alert-danger">
	                   <a href="" class="uk-alert-close uk-close"></a>
	                   <p>${msgError}</p>
	                </div>
	            </c:if>

                <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
                    <div class="uk-width-1-1">
	                    <div class="uk-panel">
	                        SUMMARY
                            <a class="uk-button uk-align-right" href="${editComponentUrl}"><i class="uk-icon-edit"></i> Edit</a>
                            <a class="uk-button uk-align-right" href="${delComponentUrl}"><i class="uk-icon-remove"></i> Delete</a>
	                        <hr>
	                        <p>
	                        Name: <c:out value="${component.name}" /><br>
	                        Description: <c:out value="${component.description}" /><br>
	                        Root Project: <a href="${rootProjectUrl}"><c:out value="${component.rootProject.name}" /></a><br>
	                        Dev Leader: <c:out value="${component.devLeader.firstName} ${component.devLeader.lastName}" /><br>
	                        QA Leader: <c:out value="${component.qaLeader.firstName} ${component.qaLeader.lastName}" /><br>
	                        </p>
	 
	                        SHARED WITH PROJECTS
                            <a class="uk-button uk-align-right" href="${shareComponentUrl}"><i class="uk-icon-edit"></i> Share</a>
                            <hr>
	                        <p>
	                        <c:if test="${not empty component.projects}">
		                        <ul>
		                        <c:forEach items="${component.projects}" var="project">
	                                <spring:url value="/admin/projects/DetailsProject" var="projectUrl" htmlEscape="true">
	                                    <spring:param name="id" value="${project.id}" />
	                                </spring:url>
	                                <spring:url value="/admin/components/UnshareProject" var="unshareProjectUrl" htmlEscape="true">
	                                    <spring:param name="cid" value="${component.id}" />
	                                    <spring:param name="pid" value="${project.id}" />
	                                </spring:url>
		                            <li><a href="${projectUrl}"><c:out value="${project.name}" /></a>
		                                <a href="${unshareProjectUrl}"><i class="uk-icon-remove"></i></a>
		                            </li>
		                        </c:forEach>
		                        </ul>
	                        </c:if>
	                        </p>
			            </div>
                    </div>
                </div>
	        </div>
	        
	    </div>
	    
	</div>
</div>

</body>
</html>