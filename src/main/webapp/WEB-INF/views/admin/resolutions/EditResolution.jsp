<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Edit Resolution</title>
    
    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/docs.css" />" type="text/css" />
</head>

<body>

<jsp:include page="../../TopNavBar.jsp" />


<!-- Add another sort of header with User's Avatar and Fullname -->
<div class="tm-middle">
	<div class="uk-container uk-container-center">
	
	    <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
            <div class="tm-sidebar uk-width-medium-1-4">
                <ul class="tm-nav uk-nav uk-nav-side" data-uk-nav>
                    <li class="uk-active"><a href="<c:url value="/admin/users/ViewUsers" />">Users</a></li>
                    <li><a href="<c:url value="/admin/GroupsList" />">Groups</a></li>
                    <li><a href="<c:url value="/admin/IssueTypeList" />">Issue Types</a></li>
                </ul>
            </div>
	        
	        <div class="tm-main uk-width-medium-3-4">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
                    <div class="uk-width-1-1">
                    <div class="uk-panel">
                    <form:form class="uk-form uk-form-horizontal" modelAttribute="resolution" method="post">
                        <div class="uk-form-row">
                            <label class="uk-form-label">
                                Name:
                                <form:input path="name"/>
                            </label>
                        </div>
		                
                        <div class="uk-form-row">
                            <label class="uk-form-label">
                                Description:
                                <form:input path="description"/>
                            </label>
                        </div>

                        <div class="uk-form-row">
                            <label class="uk-form-label">
                                Sort Value:
                                <form:input path="sortValue"/>
                            </label>
                        </div>

                        <div class="uk-form-row">
                            <label class="uk-form-label">
                                Enabled:
                                <form:checkbox path="enabled" />
                            </label>
                        </div>
                        
                        <div class="uk-form-row">
	                        <button type="submit" class="uk-button">Update</button>
	                        <a class="uk-button" href="<c:url value="/admin/resolutions/ViewResolutions" />">Cancel</a>
                        </div>
		            </form:form>
		            </div>
                    </div>
                </div>
	        </div>
	        
	    </div>
	    
	</div>
</div>

</body>
</html>