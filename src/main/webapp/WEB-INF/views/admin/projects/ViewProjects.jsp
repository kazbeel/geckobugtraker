<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Project List</title>
    
    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/docs.css" />" type="text/css" />
</head>

<body>

<jsp:include page="../../TopNavBar.jsp" />


<div class="tm-middle">
	<div class="uk-container uk-container-center">
	
	    <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
	        <div class="tm-sidebar uk-width-medium-1-4">
	            <ul class="tm-nav uk-nav uk-nav-side" data-uk-nav>
                    <li class="uk-active"><a href="<c:url value="/admin/users/ViewUsers" />">Users</a></li>
                    <li><a href="<c:url value="/admin/GroupList" />">Groups</a></li>
                    <li><a href="<c:url value="/admin/IssueTypeList" />">Issue Types</a></li>
                    <li><a href="<c:url value="/admin/IssueLinkTypeList" />">Issue Link Types</a></li>
                    <li><a href="<c:url value="/admin/PriorityList" />">Priorities</a></li>
                    <li><a href="<c:url value="/admin/ResolutionList" />">Resolutions</a></li>
                    <li><a href="<c:url value="/admin/StatusList" />">Statuses</a></li>
                    <li><a href="<c:url value="/admin/EnvironmentList" />">Environments</a></li>
                    <li><a href="<c:url value="/admin/TagList" />">Tags</a></li>
	            </ul>
	        </div>
	        
	        <div class="tm-main uk-width-medium-3-4">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
                    <div class="uk-panel uk-width-1-1">
                        <c:if test="${not empty msgSuccess}">
                            <div class="uk-alert uk-alert-success">
                               <a href="" class="uk-alert-close uk-close"></a>
                               <p>${msgSuccess}</p>
                            </div>
                        </c:if>
                        <c:if test="${not empty msgError}">
                            <div class="uk-alert uk-alert-danger">
                               <a href="" class="uk-alert-close uk-close"></a>
                               <p>${msgError}</p>
                            </div>
                        </c:if>

                        <div class="uk-panel">
                            <form:form class="uk-form uk-form-horizontal" modelAttribute="sectionForm" method="post">
	                            <form:select path="section">
	                                <form:option value="null" label="All" />
	                                <form:options items="${sections}" itemLabel="name" itemValue="id" />
	                            </form:select>
                                <button type="submit" class="uk-button">Select</button>
                            </form:form>
	                        
	                        <form action="<c:url value="/admin/projects/CreateProjectForm" />" method="post">
	                           <c:choose>
	                               <c:when test="${empty section}">
	                                   <input type="hidden" name="sectionId" value="-1">
	                               </c:when>
	                               <c:otherwise>
	                                   <input type="hidden" name="sectionId" value="${section.id}">
	                               </c:otherwise>
	                           </c:choose>
                               <button type="submit" class="uk-button uk-align-right"><i class="uk-icon-plus"></i> Create Project</button>
	                        </form>
                        </div>
                        
                        <div class="uk-panel">
                            <c:if test="${not empty section}">
                                <spring:url value="/admin/sections/DetailsSection" var="sectionUrl" htmlEscape="true">
                                    <spring:param name="id" value="${section.id}" />
                                </spring:url>
                                <h2><a href="${sectionUrl}"><c:out value="${section.name}"></c:out></a></h2>
                            </c:if>
                            ${msgError}
	                        <table class="uk-table uk-table-hover uk-table-condensed">
	                            <thead>
	                                <tr>
	                                    <th>Name</th>
	                                    <th>Section</th>
	                                    <th>Description</th>
	                                    <th>Operations</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <c:forEach items="${projects}" var="project">
                                       <spring:url value="/admin/projects/DetailsProject" var="projectUrl" htmlEscape="true">
                                           <spring:param name="id" value="${project.id}" />
                                       </spring:url>
                                       <spring:url value="/admin/sections/DetailsSection" var="sectionUrl" htmlEscape="true">
                                           <spring:param name="id" value="${project.section.id}" />
                                       </spring:url>
                                       <spring:url value="/admin/projects/EditProject" var="editProjectUrl" htmlEscape="true">
                                           <spring:param name="id" value="${project.id}" />
                                       </spring:url>
                                       <spring:url value="/admin/projects/DeleteProject" var="delProjectUrl" htmlEscape="true">
                                           <spring:param name="id" value="${project.id}" />
                                       </spring:url>
		                                <tr>
		                                    <td><a href="${projectUrl}">${project.name}</a></td>
		                                    <td><a href="${sectionUrl}">${project.section.name}</a></td>
		                                    <td>${project.description}</td>
		                                    <td>
                                              <a href="${editProjectUrl}">Edit</a>
                                              <a href="${delProjectUrl}">Delete</a>
		                                    </td>
		                                </tr>
	                                </c:forEach>
	                            </tbody>
	                        </table>
                        </div>
                    </div>
                </div>

	        </div>
	        
	    </div>
	    
	</div>
</div>

</body>
</html>