package es.kazbeel.geckobugtracker.web.utils;


import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Duration;


public class Utils {
    
    // TODO Improve this!
    public static String getDiff (Date start, Date end) {
        Duration diff = new Duration(new DateTime(start), new DateTime(end));
        
        if (diff.getStandardDays() > 0) {
            if (diff.getStandardDays() > 365) {
                return String.format("%d years ago", diff.getStandardDays() / 365);
            }
            
            if (diff.getStandardDays() > 30) {
                return String.format("%d months ago", diff.getStandardDays() / 30);
            }
            
            return String.format("%d days ago", diff.getStandardDays());
        }
        
        if (diff.getStandardHours() > 0) {
            return String.format("%d hours ago", diff.getStandardHours());
        }
        
        if (diff.getStandardMinutes() > 0) {
            return String.format("%d minutes ago", diff.getStandardMinutes());
        }
        
        return "Just Now";
    }
    
    public static String getDiff (Date start) {
        return getDiff(start, new Date());
    }
    
}
