package es.kazbeel.geckobugtracker.web.utils;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.web.secure.CurrentUserDetails;


public class SecurityUtils {
    
    public static Authentication getAuthentication () {
    
        return SecurityContextHolder.getContext().getAuthentication();
    }
    
    public static User getCurrentUser () {
    
        return ((CurrentUserDetails) getAuthentication().getPrincipal()).getCurrentUser();
    }
}
