package es.kazbeel.geckobugtracker.web.controller;


import es.kazbeel.geckobugtracker.model.Project;


public class CurrentProjectForm {
    
    private Project project = new Project();
    
    
    public CurrentProjectForm () {
    
    }
    
    public Project getProject () {
    
        return project;
    }
    
    public void setProject (Project project) {
    
        this.project = project;
    }
}
