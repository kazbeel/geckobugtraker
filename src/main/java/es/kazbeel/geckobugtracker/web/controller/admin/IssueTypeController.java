package es.kazbeel.geckobugtracker.web.controller.admin;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.IssueType;
import es.kazbeel.geckobugtracker.service.IssueService;


@Controller
public class IssueTypeController {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueTypeController.class);
    
    @Autowired
    IssueService issueService;
    
    
    @RequestMapping(value = "/admin/issuetypes/ViewIssueTypes", method = RequestMethod.GET)
    public ModelAndView viewIssueTypes () {
    
        LOG.debug("Showing List of Issue Types");
        
        ModelAndView mav = new ModelAndView("/admin/issuetypes/ViewIssueTypes");
        mav.addObject("issueTypes", issueService.getAllIssueTypes());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/issuetypes/CreateIssueType", method = RequestMethod.GET)
    public ModelAndView createIssueTypeGet () {
    
        LOG.debug("Showing Create Issue Type page");
        
        ModelAndView mav = new ModelAndView("/admin/issuetypes/CreateIssueType");
        mav.addObject("issueType", new IssueType());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/issuetypes/CreateIssueType", method = RequestMethod.POST)
    public String createIssueTypePost (@Valid @ModelAttribute("issueType") IssueType issueType, BindingResult result) {
    
        LOG.debug("Creating new Issue Type");
        
        if (result.hasErrors() == true) {
            LOG.debug("The issue type is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/issuetypes/CreateIssueType";
        }
        
        try {
            issueService.saveIssueType(issueType);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/issuetypes/CreateIssueType";
        }
        
        return "redirect:/admin/issuetypes/ViewIssueTypes";
    }
    
    @RequestMapping(value = "/admin/issuetypes/EditIssueType", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editIssueTypeGet (@RequestParam("id") Integer issueTypeId) {
    
        LOG.debug("Showing Edit Issue Type page");
        
        ModelAndView mav = new ModelAndView("/admin/issuetypes/EditIssueType");
        mav.addObject("issueType", issueService.getIssueTypeById(issueTypeId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/issuetypes/EditIssueType", params = {"id"}, method = RequestMethod.POST)
    public String editIssueTypePost (@RequestParam("id") Integer issueTypeId,
                                     @Valid @ModelAttribute("issueType") IssueType issueType,
                                     BindingResult result) {
    
        LOG.debug("Updating Issue Type (ID = {})", issueTypeId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The issue type is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/issuetypes/EditIssueType";
        }
        
        try {
            issueService.saveIssueType(issueType);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/issuetypes/EditIssueType";
        }
        
        return "redirect:/admin/issuetypes/ViewIssueTypes";
    }
    
    @RequestMapping(value = "/admin/issuetypes/DeleteIssueType", params = {"id"}, method = RequestMethod.GET)
    public String deleteIssueType (@RequestParam("id") Integer issueTypeId, RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Issue Type (ID = {})", issueTypeId);
        
        try {
            issueService.deleteIssueType(issueService.getIssueTypeById(issueTypeId));
            redirectAttributes.addFlashAttribute("msgSuccess", "Issue Type deleted successfully.");
            LOG.debug("Issue Type deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Issue Type cannot be deleted because it interacts with some issues.");
            LOG.debug("Issue Type cannot be deleted because it interacts with some issues");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Issue Type does not exist.");
            LOG.debug("Issue Type does not exist");
        }
        
        return "redirect:/admin/issuetypes/ViewIssueTypes";
    }
}
