package es.kazbeel.geckobugtracker.web.controller;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.utils.SecurityUtils;


@Controller
@RequestMapping("/profile")
@SessionAttributes("loggedUser")
public class ProfileController {
    
    private static final Logger LOG = LoggerFactory.getLogger(ProfileController.class);
    
    @Autowired
    private UserGroupService userGroupService;
    
    
    @ModelAttribute("loggedUser")
    public void populate (Model model) {
    
        model.addAttribute("loggedUser", SecurityUtils.getCurrentUser());
    }
    
    @RequestMapping(value = "/ViewProfile", method = RequestMethod.GET)
    public String viewProfileGet (@ModelAttribute("loggedUser") User loggedUser) {
    
        LOG.debug("Showing profile from {} (ID={})", loggedUser.getLoginName(), loggedUser.getId());
        
        return "/profile/ViewProfile";
    }
    
    @RequestMapping(value = "/EditProfile", method = RequestMethod.GET)
    public String editProfileGet () {
    
        return "/profile/EditProfile";
    }
    
    @RequestMapping(value = "/EditProfile", method = RequestMethod.POST)
    public String editProfilePost (@Valid @ModelAttribute("loggedUser") User loggedUser,
                                   BindingResult result,
                                   SessionStatus status) {
    
        LOG.debug("Updating user's profile (ID = {})", loggedUser.getId());
        
        if (result.hasErrors() == true) {
            LOG.debug("The user is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/profile/EditProfile";
        }
        
        userGroupService.saveUser(loggedUser);
        status.setComplete();
        
        return "redirect:/profile/ViewProfile";
    }
}
