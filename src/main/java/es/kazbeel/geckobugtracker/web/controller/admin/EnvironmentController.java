package es.kazbeel.geckobugtracker.web.controller.admin;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.service.IssueService;


@Controller
@RequestMapping("/admin/environments/")
public class EnvironmentController {
    
    private static final Logger LOG = LoggerFactory.getLogger(EnvironmentController.class);
    
    private static final String BASE_URL = "/admin/environments/";
    
    private static final String VIEW_URL = BASE_URL + "ViewEnvironments";
    
    private static final String CREATE_URL = BASE_URL + "CreateEnvironment";
    
    private static final String EDIT_URL = BASE_URL + "EditEnvironment";
    
    @Autowired
    private IssueService issueService;
    
    
    @RequestMapping(value = "/ViewEnvironments", method = RequestMethod.GET)
    public ModelAndView viewEnvironments () {
    
        LOG.debug("Showing List of Environments View");
        
        ModelAndView mav = new ModelAndView(VIEW_URL);
        mav.addObject("environments", issueService.getAllEnvironments());
        
        return mav;
    }
    
    @RequestMapping(value = "/CreateEnvironment", method = RequestMethod.GET)
    public ModelAndView createEnvironmentGet () {
    
        LOG.debug("Showing Create Environment View");
        
        ModelAndView mav = new ModelAndView(CREATE_URL);
        mav.addObject("environment", new Environment());
        
        return mav;
    }
    
    @RequestMapping(value = "/CreateEnvironment", method = RequestMethod.POST)
    public String createEnvironmentPost (@Valid @ModelAttribute("environment") Environment environment,
                                         BindingResult result) {
    
        LOG.debug("Creating new Environment");
        
        if (result.hasErrors() == true) {
            LOG.debug("The environment is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return CREATE_URL;
        }
        
        try {
            issueService.saveEnvironment(environment);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return CREATE_URL;
        }
        
        return "redirect:" + VIEW_URL;
    }
    
    @RequestMapping(value = "/EditEnvironment", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editEnvironmentGet (@RequestParam("id") Integer environmentId) {
    
        LOG.debug("Showing Edit Environment page (ID = {})", environmentId);

        Environment environment = issueService.getEnvironmentById(environmentId);
        
        if (environment == null) {
            LOG.debug("Environment (ID = {}) does not exist", environmentId);
            
            return new ModelAndView("redirect:" + VIEW_URL);
        }
        
        ModelAndView mav = new ModelAndView(EDIT_URL);
        mav.addObject("environment", environment);
        
        return mav;
    }
    
    @RequestMapping(value = "/EditEnvironment", params = {"id"}, method = RequestMethod.POST)
    public String editEnvironmentPost (@RequestParam("id") Integer environmentId,
                                       @Valid @ModelAttribute("environment") Environment environment,
                                       BindingResult result) {
    
        LOG.debug("Updating Environment (ID = {})", environmentId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The environment is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return EDIT_URL;
        }
        
        try {
            issueService.saveEnvironment(environment);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return EDIT_URL;
        }
        
        return "redirect:" + VIEW_URL;
    }
    
    @RequestMapping(value = "/DeleteEnvironment", params = {"id"}, method = RequestMethod.GET)
    public String deleteEnvironment (@RequestParam("id") Integer environmentId,
                                     RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Environment (ID = {})", environmentId);
        
        try {
            Environment environment = issueService.getEnvironmentById(environmentId);
            issueService.deleteEnvironment(environment);
            
            redirectAttributes.addFlashAttribute("msgSuccess", "Environment deleted successfully.");
            
            LOG.debug("Environment deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Environment cannot be deleted because it's used by some issues.");
            
            LOG.debug("Environment cannot be deleted because it's used by some issues");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Environment does not exist.");
            
            LOG.debug("Environment does not exist");
        }
        
        return "redirect:" + VIEW_URL;
    }
}
