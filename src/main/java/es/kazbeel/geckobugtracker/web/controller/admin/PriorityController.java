package es.kazbeel.geckobugtracker.web.controller.admin;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Priority;
import es.kazbeel.geckobugtracker.service.IssueService;


@Controller
public class PriorityController {
    
    private static final Logger LOG = LoggerFactory.getLogger(PriorityController.class);
    
    @Autowired
    IssueService issueService;
    
    
    @RequestMapping(value = "/admin/priorities/ViewPriorities", method = RequestMethod.GET)
    public ModelAndView viewPriorities () {
    
        LOG.debug("Showing List of Priorities");
        
        ModelAndView mav = new ModelAndView("/admin/priorities/ViewPriorities");
        mav.addObject("priorities", issueService.getAllPriorities());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/priorities/CreatePriority", method = RequestMethod.GET)
    public ModelAndView createPriorityGet () {
    
        LOG.debug("Showing Priority Create page");
        
        ModelAndView mav = new ModelAndView("/admin/priorities/CreatePriority");
        mav.addObject("priority", new Priority());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/priorities/CreatePriority", method = RequestMethod.POST)
    public String createPriorityPost (@Valid @ModelAttribute("priority") Priority priority, BindingResult result) {
    
        LOG.debug("Creating new Priority");
        
        if (result.hasErrors() == true) {
            LOG.debug("The priority is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/priorities/CreatePriority";
        }
        
        try {
            issueService.savePriority(priority);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/priorities/CreatePriority";
        }
        
        return "redirect:/admin/priorities/ViewPriorities";
    }
    
    @RequestMapping(value = "/admin/priorities/EditPriority", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editPriorityGet (@RequestParam("id") Integer priorityId) {
    
        LOG.debug("Showing Edit Priority page");
        
        ModelAndView mav = new ModelAndView("/admin/priorities/EditPriority");
        mav.addObject("priority", issueService.getPriorityById(priorityId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/priorities/EditPriority", params = {"id"}, method = RequestMethod.POST)
    public String editPriorityPost (@RequestParam("id") Integer priorityId,
                                    @Valid @ModelAttribute("priority") Priority priority,
                                    BindingResult result) {
    
        LOG.debug("Updating Priority (ID = {})", priorityId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The priority is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/priorities/EditPriority";
        }
        
        try {
            issueService.savePriority(priority);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/priorities/EditPriority";
        }
        
        return "redirect:/admin/priorities/ViewPriorities";
    }
    
    @RequestMapping(value = "/admin/priorities/DeletePriority", params = {"id"}, method = RequestMethod.GET)
    public String deletePriority (@RequestParam("id") Integer priorityId,
                                  RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Priority (ID = {})", priorityId);
        
        try {
            issueService.deletePriority(issueService.getPriorityById(priorityId));
            redirectAttributes.addFlashAttribute("msgSuccess", "Priority deleted successfully.");
            LOG.debug("Priority deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Priority cannot be deleted because it interacts with some issues.");
            LOG.debug("Priority cannot be deleted because it interacts with some issues");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Priority does not exist.");
            LOG.debug("Priority does not exist");
        }
        
        return "redirect:/admin/priorities/ViewPriorities";
    }
    
    @RequestMapping(value = "/admin/priorities/SortPriority", params = {"id", "move"}, method = RequestMethod.GET)
    public String setPriorityOrder (@RequestParam("id") Integer priorityId,
                                    @RequestParam("move") String move,
                                    RedirectAttributes redirectAttributes) {
    
        LOG.debug("Move Priority {} (ID = {})", move, priorityId);
        
        Priority priority = issueService.getPriorityById(priorityId);
        
        if (priority == null) {
            redirectAttributes.addFlashAttribute("msgError", "The priority does not exist.");
            
            LOG.debug("The priority (ID = {}) does not exist", priorityId);
        } else {
            if (move.equalsIgnoreCase("up")) {
                issueService.setHigherOrderForPriority(priority);
            } else if (move.equalsIgnoreCase("down")) {
                issueService.setLowerOrderForPriority(priority);
            }
        }
        
        return "redirect:/admin/priorities/ViewPriorities";
    }
}
