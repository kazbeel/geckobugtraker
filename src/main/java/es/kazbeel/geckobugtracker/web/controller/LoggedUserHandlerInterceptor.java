package es.kazbeel.geckobugtracker.web.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import es.kazbeel.geckobugtracker.web.utils.SecurityUtils;


public class LoggedUserHandlerInterceptor extends HandlerInterceptorAdapter {
    
    private static final Logger LOG = LoggerFactory.getLogger(LoggedUserHandlerInterceptor.class);
    
    
    public LoggedUserHandlerInterceptor () {
    
    }
    
    @Override
    public void postHandle (HttpServletRequest request,
                            HttpServletResponse response,
                            Object handler,
                            ModelAndView modelAndView) {
    
        if (modelAndView != null) {
            LOG.debug("Adding model's attribute <loggedUser> for view {}", modelAndView.getViewName());
            
            modelAndView.getModelMap().addAttribute("loggedUser", SecurityUtils.getCurrentUser());
        }
    }
}
