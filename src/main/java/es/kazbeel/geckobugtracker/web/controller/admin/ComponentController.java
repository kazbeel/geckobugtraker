package es.kazbeel.geckobugtracker.web.controller.admin;


import java.util.Collection;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Component;
import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.ProjectService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.controller.exception.ResourceNotFoundException;
import es.kazbeel.geckobugtracker.web.entityeditors.ProjectEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.UserEditor;


@Controller
public class ComponentController {
    
    private static final Logger LOG = LoggerFactory.getLogger(ComponentController.class);
    
    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private UserGroupService userGroupService;
    
    
    @InitBinder
    public void initBinder (WebDataBinder binder) {
    
        binder.registerCustomEditor(Project.class, new ProjectEditor(projectService));
        binder.registerCustomEditor(User.class, new UserEditor(userGroupService));
    }
    
    @RequestMapping(value = "/admin/components/CreateComponentForm", method = RequestMethod.POST)
    public ModelAndView createComponentForm (@ModelAttribute("projectId") Long projectId) {
    
        LOG.debug("Showing Create Component view (preassigned Project = {})", projectId);
        
        ModelAndView mav = new ModelAndView("/admin/components/CreateComponent");
        mav.addObject("users", userGroupService.getAllEnabledUsers());
        mav.addObject("project", projectService.getProjectById(projectId));
        mav.addObject("component", new Component());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/components/CreateComponent", method = RequestMethod.POST)
    public String createComponentPost (@Valid @ModelAttribute("component") Component component,
                                       BindingResult result,
                                       RedirectAttributes redirectAttributes) {
    
        LOG.debug("Creating new Component");
        
        if (result.hasErrors() == true) {
            LOG.debug("The component is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/components/CreateComponent";
        }
        
        try {
            projectService.saveComponent(component);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/components/CreateComponent";
        }
        
        redirectAttributes.addAttribute("id", component.getRootProject().getId());
        return "redirect:/admin/projects/DetailsProject";
    }
    
    @RequestMapping(value = "/admin/components/EditComponent", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editComponentGet (@RequestParam("id") Long componentId) {
    
        LOG.debug("Showing Edit Component view (ID = {})", componentId);
        
        ModelAndView mav = new ModelAndView("/admin/components/EditComponent");
        mav.addObject("users", userGroupService.getAllEnabledUsers());
        mav.addObject("projects", projectService.getAllProjects());
        mav.addObject("component", projectService.getComponentById(componentId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/components/EditComponent", params = {"id"}, method = RequestMethod.POST)
    public String editComponentPost (@RequestParam("id") Long componentId,
                                     @Valid @ModelAttribute("component") Component component,
                                     BindingResult result,
                                     RedirectAttributes redirectAttributes) {
    
        LOG.debug("Updating Component (ID = {})", componentId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The component is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/components/EditComponent";
        }
        
        try {
            projectService.saveComponent(component);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/components/EditComponent";
        }
        
        redirectAttributes.addAttribute("id", component.getId());
        return "redirect:/admin/components/DetailsComponent";
    }
    
    @RequestMapping(value = "/admin/components/DeleteComponent", params = {"id"}, method = RequestMethod.GET)
    public String deleteComponent (@RequestParam("id") Long componentId, RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Component (ID = {})", componentId);
        
        try {
            Component component = projectService.getComponentById(componentId);
            
            projectService.deleteComponent(component);
            
            redirectAttributes.addFlashAttribute("msgSuccess", "Component deleted successfully.");
            redirectAttributes.addAttribute("id", component.getRootProject().getId());
            
            LOG.debug("Component deleted successfully");
            
            return "redirect:/admin/projects/DetailsProject";
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Component cannot be deleted because it interacts with some projects.");
            LOG.debug("Component cannot be deleted because it interacts with some projects");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Component does not exist.");
            LOG.debug("Component cannot be deleted because it interacts with some projects");
        }
        
        return "redirect:/admin/projects/ViewProjects";
    }
    
    @RequestMapping(value = "/admin/components/DetailsComponent", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView detailsComponent (@RequestParam("id") Long componentId) {
    
        LOG.debug("Showing Details of Component (ID = {})", componentId);
        
        ModelAndView mav = new ModelAndView("/admin/components/DetailsComponent");
        
        Component component = projectService.getComponentByIdWithSharedProjects(componentId);
        
        if (component == null) {
            LOG.debug("Requested Component does not exist");
            
            throw new ResourceNotFoundException();
        }
        
        mav.addObject("component", component);
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/components/ShareComponent", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView shareComponentGet (@RequestParam("id") Long componentId, RedirectAttributes redirectAttributes) {
    
        LOG.debug("Showing Share Component view (preassigned Component = {})", componentId);
        
        ModelAndView mav = new ModelAndView("/admin/components/ShareComponent");
        
        Component component = projectService.getComponentById(componentId);
        
        if (component == null) {
            LOG.debug("Requested Component does not exist");
            
            throw new ResourceNotFoundException();
        }
        
        Collection<Project> shareableProjects = projectService.getShareableProjectsForComponent(component);
        
        if (shareableProjects.isEmpty() == true) {
            redirectAttributes.addFlashAttribute("msgError", "There are not projects to share with.");
            redirectAttributes.addAttribute("id", componentId);
            mav.setViewName("redirect:/admin/components/DetailsComponent");
        } else {
            mav.addObject("componentId", componentId);
            mav.addObject("projects", shareableProjects);
            mav.addObject("shareComponentForm", new ShareComponentForm());
        }
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/components/ShareComponent", method = RequestMethod.POST)
    public String shareComponentPost (@ModelAttribute("shareComponentForm") ShareComponentForm shareComponentForm,
                                      RedirectAttributes redirectAttributes) {
    
        LOG.debug("Sharing Component (ID = {}) with Project (ID = {})",
                  shareComponentForm.getComponentId(),
                  shareComponentForm.getProject().getId());
        
        Component component = projectService.getComponentById(shareComponentForm.getComponentId());
        Project project = projectService.getProjectByIdWithSharedComponentsAndVersions(shareComponentForm.getProject()
                                                                                                         .getId());
        
        projectService.shareComponentWithProject(component, project);
        
        redirectAttributes.addAttribute("id", shareComponentForm.getComponentId());
        
        return "redirect:/admin/components/DetailsComponent";
    }
    
    @RequestMapping(value = "/admin/components/UnshareProject", params = {"cid", "pid"}, method = RequestMethod.GET)
    public String unshareComponentPost (@RequestParam("cid") Long componentId,
                                        @RequestParam("pid") Long projectId,
                                        RedirectAttributes redirectAttributes) {
    
        LOG.debug("Unsharing Component (ID = {}) from Project (ID = {})", componentId, projectId);
        
        Component component = projectService.getComponentById(componentId);
        Project project = projectService.getProjectByIdWithSharedComponentsAndVersions(projectId);
        
        projectService.unshareCompronentFromProject(component, project);
        
        redirectAttributes.addAttribute("id", componentId);
        
        return "redirect:/admin/components/DetailsComponent";
    }
}
