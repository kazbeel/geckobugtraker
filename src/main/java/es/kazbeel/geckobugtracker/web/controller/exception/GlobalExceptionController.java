package es.kazbeel.geckobugtracker.web.controller.exception;


import java.io.FileNotFoundException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice
public class GlobalExceptionController {
    
    private static final Logger LOG = LoggerFactory.getLogger(GlobalExceptionController.class);
    
    
    @ExceptionHandler(CannotCreateTransactionException.class)
    @ResponseStatus(HttpStatus.CREATED)
    public ModelAndView handleCannotCreateTransactionException (HttpServletRequest req, Exception ex) {
    
        LOG.debug("Handling CannotCreateTransactionException thrown from " + req.getRequestURL());
        
        ModelAndView mav = new ModelAndView("errors/GenericError");
        mav.addObject("msg", ex.getMessage());
        
        return mav;
    }
    
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    @ResponseStatus(HttpStatus.CREATED)
    public ModelAndView handleMaxUploadSizeExceededException (HttpServletRequest req, Exception ex) {
    
        LOG.debug("Handling MaxUploadSizeExceededException thrown from " + req.getRequestURL());
        
        ModelAndView mav = new ModelAndView("errors/GenericError");
        mav.addObject("msg", ex.getMessage());
        
        return mav;
    }
    
    @ExceptionHandler({MissingServletRequestParameterException.class,
                       UnsatisfiedServletRequestParameterException.class,
                       HttpRequestMethodNotSupportedException.class,
                       TypeMismatchException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView handleHttpError400 (HttpServletRequest req, Exception ex) {
    
        LOG.debug("Handling HTTP Error 400 thrown from " + req.getRequestURL() + "?" + req.getQueryString());
        
        ModelAndView mav = new ModelAndView("errors/GenericError");
        mav.addObject("title", "Bad Request");
        mav.addObject("description",
                      "The server cannot or will not process the request due to something that is perceived to be a client error.");
        
        return mav;
    }
    
    @ExceptionHandler(FileNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView handleFileNotFoundException (HttpServletRequest req, Exception ex) {
        
        LOG.debug("Handling FileNotFoundException thrown from " + req.getRequestURL());
        
        ModelAndView mav = new ModelAndView("errors/GenericError");
        mav.addObject("title", HttpStatus.NOT_FOUND.name());
        mav.addObject("description", ex.getMessage());
        
        return mav;
    }
}
