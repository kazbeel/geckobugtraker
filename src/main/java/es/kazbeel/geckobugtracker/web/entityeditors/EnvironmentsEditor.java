package es.kazbeel.geckobugtracker.web.entityeditors;


import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.service.IssueService;


public class EnvironmentsEditor extends CustomCollectionEditor {
    
    private static final Logger LOG = LoggerFactory.getLogger(EnvironmentsEditor.class);
    
    private IssueService        issueService;
    
    
    @SuppressWarnings("rawtypes")
    public EnvironmentsEditor (Class<? extends Collection> collectionType, IssueService issueService) {
    
        super(collectionType, false);
        
        this.issueService = issueService;
    }
    
    @Override
    protected Object convertElement (Object element) {
    
        String id = (String) element;
        Environment env = issueService.getEnvironmentById(Integer.parseInt(id));
        
        if (env == null) {
            LOG.error("Previously set Environment has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("Environment with ID=" + env.getName() + " is returned");
        
        return env;
    }
}
