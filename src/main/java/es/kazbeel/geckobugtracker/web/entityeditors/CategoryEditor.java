package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.Category;
import es.kazbeel.geckobugtracker.service.IssueService;


public class CategoryEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(CategoryEditor.class);
    
    private IssueService        issueService;
    
    
    public CategoryEditor (IssueService issueService) {
    
        this.issueService = issueService;
    }
    
    @Override
    public void setAsText (String id) {
    
        if (id.equals("null")) {
            this.setValue(null);
        } else {
            Category category = issueService.getCategoryById(Integer.parseInt(id));
            
            if (category == null) {
                LOG.error("Category with ID=" + id + " does not exist");
            } else {
                this.setValue(category);
            }
        }
        
        LOG.debug("Set Category (ID=" + id + ") as text");
    }
    
    @Override
    public String getAsText () {
    
        Category category = (Category) this.getValue();
        
        if (category == null) {
            LOG.debug("No Category was set so \"null\" is returned");
            
            return "null";
        }
        
        LOG.debug("Get Category with ID=" + category.getId() + " is returned");
        
        return category.getId().toString();
    }
    
}
