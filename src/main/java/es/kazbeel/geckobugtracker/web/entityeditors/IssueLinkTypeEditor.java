package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.IssueLinkType;
import es.kazbeel.geckobugtracker.service.IssueService;


public class IssueLinkTypeEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueLinkTypeEditor.class);
    
    private IssueService        issueService;
    
    
    public IssueLinkTypeEditor (IssueService issueService) {
    
        this.issueService = issueService;
    }
    
    @Override
    public void setAsText (String id) {
    
        LOG.debug("Set ID=" + id + " as text");
        IssueLinkType issueLinkType = issueService.getIssueLinkTypeById(Integer.parseInt(id));
        this.setValue(issueLinkType);
    }
    
    @Override
    public String getAsText () {
    
        IssueLinkType issueLinkType = (IssueLinkType) this.getValue();
        
        if (issueLinkType == null) {
            LOG.error("Previously set IssueLinkType has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("IssueLinkType with ID=" + issueLinkType.getName() + " is returned");
        
        return issueLinkType.getName();
    }
}
