package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.service.IssueService;


public class ResolutionEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(ResolutionEditor.class);
    
    private IssueService        issueService;
    
    
    public ResolutionEditor (IssueService issueService) {
    
        this.issueService = issueService;
    }
    
    @Override
    public void setAsText (String id) {
    
        LOG.debug("Set ID=" + id + " as text");
        Resolution resolution = issueService.getResolutionById(Integer.parseInt(id));
        this.setValue(resolution);
    }
    
    @Override
    public String getAsText () {
    
        Resolution resolution = (Resolution) this.getValue();
        
        if (resolution == null) {
            LOG.error("Previously set Resolution has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("Resolution with ID=" + resolution.getName() + " is returned");
        
        return resolution.getName();
    }
    
}
