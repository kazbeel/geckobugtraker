package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.service.ProjectService;


public class ProjectEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(ProjectEditor.class);
    
    private ProjectService projectService;
    
    
    public ProjectEditor (ProjectService projectService) {
    
        this.projectService = projectService;
    }
    
    @Override
    public void setAsText (String id) {
    
        if (id.equals("null")) {
            this.setValue(null);
        } else {
            Project project = projectService.getProjectById(Long.parseLong(id));
            
            if (project == null) {
                LOG.error("Project with ID=" + id + " does not exist");
            } else {
                this.setValue(project);
            }
        }
        
        LOG.debug("Set Project (ID=" + id + ") as text");
    }
    
    @Override
    public String getAsText () {
    
        Project project = (Project) this.getValue();
        
        if (project == null) {
            LOG.debug("No Project was set so \"null\" is returned");
            
            return "null";
        }
        
        LOG.debug("Get Project with ID=" + project.getId() + " is returned");
        
        return project.getId().toString();
    }
    
}
