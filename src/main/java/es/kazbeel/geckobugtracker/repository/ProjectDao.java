package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Project;


public interface ProjectDao extends BaseDao<Project> {
    
    public Project findById (Long projectId);
    
    public List<Project> findAll ();
    
    public List<Project> findBySection (Long sectionId);
    
    public Project findByName (String name);
    
    public List<Project> findByDevLeader (Integer id);
    
    public List<Project> findByQALeader (Integer id);
    
    public List<Project> findShareableProjectsForComponent (Long componentId);
}
