package es.kazbeel.geckobugtracker.repository;


import es.kazbeel.geckobugtracker.model.Comment;


public interface CommentDao extends BaseDao<Comment> {
    
}
