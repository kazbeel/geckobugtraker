package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.User;


public interface UserDao extends BaseDao<User> {
    
    public List<User> findAll (int offset, int size);
    
    public User findById (Integer id);
    
    public User findByLoginName (String loginName);
    
    public List<User> findAllEnabled ();
    
    public Long countAll ();
}
