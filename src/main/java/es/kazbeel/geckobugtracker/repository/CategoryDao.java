package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Category;


public interface CategoryDao extends BaseDao<Category> {
    
    public List<Category> findAll ();
    
    public Category findById (Integer id);
}
