package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.repository.UserDao;


@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<User> findAll (int offset, int size) {
    
        return getSession().getNamedQuery("User_findAll").setFirstResult(offset).setMaxResults(size).list();
    }
    
    @Override
    public User findById (Integer id) {
    
        return (User) getSession().get(User.class, id);
    }
    
    @Override
    public User findByLoginName (String loginName) {
    
        return (User) getSession().getNamedQuery("User_findByLoginName")
                                  .setString("loginName", loginName)
                                  .uniqueResult();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<User> findAllEnabled () {
    
        return getSession().getNamedQuery("User_findAllEnabled").list();
    }

    @Override
    public Long countAll () {
    
        return (Long) getSession().getNamedQuery("User_countAll").uniqueResult();
    }
}
