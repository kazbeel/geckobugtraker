package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.repository.TagDao;


@Repository
public class TagDaoImpl extends BaseDaoImpl<Tag> implements TagDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Tag> findAll () {
    
        return getSession().getNamedQuery("Tag_findAll").list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Tag> findAllEnabled () {
    
        return getSession().getNamedQuery("Tag_findAllEnabled").list();
    }
    
    @Override
    public Tag findById (Integer id) {
    
        return (Tag) getSession().get(Tag.class, id);
    }
}
