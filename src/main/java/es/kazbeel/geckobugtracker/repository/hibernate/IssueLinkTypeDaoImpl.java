package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.IssueLinkType;
import es.kazbeel.geckobugtracker.repository.IssueLinkTypeDao;


@Repository
public class IssueLinkTypeDaoImpl extends BaseDaoImpl<IssueLinkType> implements IssueLinkTypeDao {

    @Override
    @SuppressWarnings("unchecked")
    public List<IssueLinkType> findAll () {
    
        return getSession().getNamedQuery("IssueLinkType_findAll").list();
    }

    @Override
    public IssueLinkType findById (Integer id) {
    
        return (IssueLinkType) getSession().get(IssueLinkType.class, id);
    }
    
}
