package es.kazbeel.geckobugtracker.repository.hibernate;


import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.repository.BaseDao;


@Repository
public abstract class BaseDaoImpl<T extends Serializable> implements BaseDao<T> {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    public void save (T entity) {
    
        getSession().save(entity);
    }
    
    public void saveOrUpdate (T entity) {
    
        getSession().saveOrUpdate(entity);
    }
    
    public void delete (T entity) {
    
        getSession().delete(entity);
    }
    
    public void refresh (T entity) {
    
        getSession().refresh(entity);
    }
    
    protected Session getSession () {
    
        return sessionFactory.getCurrentSession();
    }
    
}
