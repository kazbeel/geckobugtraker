package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Group;
import es.kazbeel.geckobugtracker.repository.GroupDao;


@Repository
public class GroupDaoImpl extends BaseDaoImpl<Group> implements GroupDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Group> findAll (int offset, int size) {
    
        return getSession().getNamedQuery("Group_findAll").setFirstResult(offset).setMaxResults(size).list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Group> findAllEnabled () {
    
        return getSession().getNamedQuery("Group_findAllEnabled").list();
    }
    
    @Override
    public Group findById (Integer id) {
    
        return (Group) getSession().get(Group.class, id);
    }
    
    @Override
    public Long countAll () {
    
        return (Long) getSession().getNamedQuery("Group_countAll").uniqueResult();
    }
}
