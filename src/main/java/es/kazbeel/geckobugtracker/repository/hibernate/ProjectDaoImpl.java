package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.repository.ProjectDao;


@Repository
public class ProjectDaoImpl extends BaseDaoImpl<Project> implements ProjectDao {
    
    @Override
    public Project findById (Long projectId) {
    
        return (Project) getSession().get(Project.class, projectId);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Project> findAll () {
    
        return getSession().getNamedQuery("Project.findAll").list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Project> findBySection (Long sectionId) {
    
        return getSession().getNamedQuery("Project.findBySectionId").setLong("id", sectionId).list();
    }
    
    @Override
    public Project findByName (String name) {
    
        return (Project) getSession().getNamedQuery("Project.findByName").setString("name", name).uniqueResult();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Project> findByDevLeader (Integer id) {
    
        return getSession().getNamedQuery("Project.findByDevLeader").setLong("id", id).list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Project> findByQALeader (Integer id) {
    
        return getSession().getNamedQuery("Project.findByQALeader").setLong("id", id).list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Project> findShareableProjectsForComponent (Long componentId) {
    
        return getSession().getNamedQuery("Project.findShareableProjects").setLong("cid", componentId).list();
    }
}
