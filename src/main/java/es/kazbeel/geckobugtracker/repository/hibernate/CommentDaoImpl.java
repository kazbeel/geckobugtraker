package es.kazbeel.geckobugtracker.repository.hibernate;


import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Comment;
import es.kazbeel.geckobugtracker.repository.CommentDao;


@Repository
public class CommentDaoImpl extends BaseDaoImpl<Comment> implements CommentDao {
    
}
