package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.RemembermeToken;
import es.kazbeel.geckobugtracker.repository.RemembermeTokenDao;


@Repository
public class RemembermeTokenDaoImpl extends BaseDaoImpl<RemembermeToken> implements RemembermeTokenDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<RemembermeToken> findByUsername (String username) {
    
        return getSession().getNamedQuery("RemembermeToken_findByUsername")
                           .setString("username", username)
                           .list();
    }
    
    @Override
    public RemembermeToken findBySeries (String series) {
    
        return (RemembermeToken) getSession().getNamedQuery("RemembermeToken_findBySeries")
                                             .setString("series", series)
                                             .uniqueResult();
    }
}
