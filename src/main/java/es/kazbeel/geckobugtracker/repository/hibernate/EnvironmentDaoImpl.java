package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.repository.EnvironmentDao;


@Repository
public class EnvironmentDaoImpl extends BaseDaoImpl<Environment> implements EnvironmentDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Environment> findAll () {
    
        return getSession().getNamedQuery("Environment_findAll").list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Environment> findAllEnabled () {
    
        return getSession().getNamedQuery("Environment_findAllEnabled").list();
    }
    
    @Override
    public Environment findById (Integer id) {
    
        return (Environment) getSession().get(Environment.class, id);
    }
}
