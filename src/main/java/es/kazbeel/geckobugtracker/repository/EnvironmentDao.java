package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Environment;


public interface EnvironmentDao extends BaseDao<Environment> {
    
    public List<Environment> findAll ();
    
    public List<Environment> findAllEnabled ();
    
    public Environment findById (Integer id);
}
