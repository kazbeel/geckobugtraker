package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Attachment;


public interface AttachmentDao extends BaseDao<Attachment> {
    
    public Attachment findById (Integer attachmentId);

    public List<Attachment> findAllFromIssue (Integer issueId);
}
