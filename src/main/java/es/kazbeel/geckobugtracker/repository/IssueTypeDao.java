package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.IssueType;


public interface IssueTypeDao extends BaseDao<IssueType> {
    
    public List<IssueType> findAll ();
    
    public List<IssueType> findAllEnabled ();
    
    public IssueType findById (Integer id);
}
