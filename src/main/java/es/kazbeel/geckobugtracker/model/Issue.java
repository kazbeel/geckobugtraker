package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class Issue implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    private Integer id;
    
    private Component component;
    
    @NotNull
    private IssueType type;
    
    @NotNull
    private User reporter;
    
    private User assignee;
    
    private Priority priority;
    
    private Status status;
    
    private Resolution resolution;
    
    private Category category;
    
    private Set<Environment> environments = new HashSet<Environment>(0);
    
    @NotNull
    @Size(min = 1, max = 255)
    private String summary;
    
    @Size(max = 100000)
    private String description;
    
    private Set<Tag> tags = new HashSet<Tag>(0);
    
    private Set<Comment> comments = new HashSet<Comment>(0);
    
    private Date createdOn;
    
    private Date lastUpdateOn;
    
    
    public Issue () {
    
    }
    
    public Component getComponent () {
    
        return component;
    }
    
    public void setComponent (Component component) {
    
        this.component = component;
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public IssueType getType () {
    
        return type;
    }
    
    public void setType (IssueType type) {
    
        this.type = type;
    }
    
    public User getReporter () {
    
        return reporter;
    }
    
    public void setReporter (User reporter) {
    
        this.reporter = reporter;
    }
    
    public User getAssignee () {
    
        return assignee;
    }
    
    public void setAssignee (User assignee) {
    
        this.assignee = assignee;
    }
    
    public Priority getPriority () {
    
        return priority;
    }
    
    public void setPriority (Priority priority) {
    
        this.priority = priority;
    }
    
    public Status getStatus () {
    
        return status;
    }
    
    public void setStatus (Status status) {
    
        this.status = status;
    }
    
    public Resolution getResolution () {
    
        return resolution;
    }
    
    public void setResolution (Resolution resolution) {
    
        this.resolution = resolution;
    }
    
    public Category getCategory () {
    
        return category;
    }
    
    public void setCategory (Category category) {
    
        this.category = category;
    }
    
    public Set<Environment> getEnvironments () {
    
        return environments;
    }
    
    public void setEnvironments (Set<Environment> environment) {
    
        this.environments = environment;
    }
    
    public void addEnvironment (Environment environment) {
    
        environments.add(environment);
    }
    
    public String getSummary () {
    
        return summary;
    }
    
    public void setSummary (String summary) {
    
        this.summary = summary;
    }
    
    public String getDescription () {
    
        return description;
    }
    
    public void setDescription (String description) {
    
        this.description = description;
    }
    
    public Set<Tag> getTags () {
    
        return tags;
    }
    
    public void setTags (Set<Tag> tags) {
    
        this.tags = tags;
    }
    
    public void addTag (Tag tag) {
    
        tags.add(tag);
    }
    
    public Set<Comment> getComments () {
    
        return comments;
    }
    
    public void setComments (Set<Comment> comments) {
    
        this.comments = comments;
    }
    
    public Date getCreatedOn () {
    
        return createdOn;
    }
    
    public void setCreatedOn (Date createdOn) {
    
        this.createdOn = createdOn;
    }
    
    public Date getLastUpdateOn () {
    
        return lastUpdateOn;
    }
    
    public void setLastUpdateOn (Date lastUpdateOn) {
    
        this.lastUpdateOn = lastUpdateOn;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Issue other = (Issue) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
