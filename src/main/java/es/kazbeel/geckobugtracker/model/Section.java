package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "SECTIONS")
@NamedQueries({@NamedQuery(name = "Section.findByName", query = "FROM Section WHERE name = :name"),
               @NamedQuery(name = "Section.decreaseSortValue", query = "UPDATE Section SET sortValue = CASE WHEN (sortValue > 0) THEN (sortValue - 1) ELSE (sortValue) END WHERE id = :id"),
               @NamedQuery(name = "Section.increaseSortValue", query = "UPDATE Section SET sortValue = (sortValue + 1) WHERE id = :id")})
public class Section implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NAME", length = 255, unique = true, nullable = false)
    private String name;
    
    @Size(max = 4096)
    @Column(name = "DESCRIPTION", length = 4096)
    private String description;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "SORT_VALUE", nullable = false)
    private Integer sortValue = 0;
    
    @OneToMany(mappedBy = "section", cascade = CascadeType.REMOVE)
    private Set<Project> projects = new HashSet<Project>(0);
    
    
    public Section () {
    
    }
    
    public Long getId () {
    
        return id;
    }
    
    public void setId (Long id) {
    
        this.id = id;
    }
    
    public String getName () {
    
        return name;
    }
    
    public void setName (String name) {
    
        this.name = name;
    }
    
    public String getDescription () {
    
        return description;
    }
    
    public void setDescription (String description) {
    
        this.description = description;
    }
    
    public Integer getSortValue () {
    
        return sortValue;
    }
    
    public void setSortValue (Integer sortValue) {
    
        this.sortValue = sortValue;
    }
    
    public Set<Project> getProjects () {
    
        return projects;
    }
    
    public void setProjects (Set<Project> projects) {
    
        this.projects = projects;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Section))
            return false;
        Section other = (Section) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
