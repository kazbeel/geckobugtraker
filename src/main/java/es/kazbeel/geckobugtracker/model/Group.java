package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class Group implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    private Integer id;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String name;
    
    @Size(max = 1000)
    private String description;
    
    @NotNull
    private boolean enabled = true;
    
    private Set<User> users = new HashSet<User>(0);
    
    
    public Group () {
    
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public String getName () {
    
        return name;
    }
    
    public void setName (String name) {
    
        this.name = name;
    }
    
    public boolean isEnabled () {
    
        return enabled;
    }
    
    public void setEnabled (boolean enabled) {
    
        this.enabled = enabled;
    }
    
    public String getDescription () {
    
        return description;
    }
    
    public void setDescription (String description) {
    
        this.description = description;
    }
    
    public Set<User> getUsers () {
    
        return users;
    }
    
    public void setUsers (Set<User> users) {
    
        this.users = users;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Group other = (Group) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
