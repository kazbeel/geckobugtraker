package es.kazbeel.geckobugtracker.model;

import java.io.Serializable;


public class Vote extends IssueActor implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    public Vote () {
        super();
    }
    
    public Vote (User user, Issue issue) {
        super(user, issue);
    }
    
}
