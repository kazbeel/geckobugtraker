package es.kazbeel.geckobugtracker.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;


public class IssueLink implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    private Integer id;
    
    @NotNull
    private Issue source;
    
    @NotNull
    private Issue destination;
    
    @NotNull
    private IssueLinkType linkType;
    
    
    public IssueLink () {
    
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public Issue getSource () {
    
        return source;
    }
    
    public void setSource (Issue source) {
    
        this.source = source;
    }
    
    public Issue getDestination () {
    
        return destination;
    }
    
    public void setDestination (Issue destination) {
    
        this.destination = destination;
    }
    
    public IssueLinkType getLinkType () {
    
        return linkType;
    }
    
    public void setLinkType (IssueLinkType linkType) {
    
        this.linkType = linkType;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IssueLink other = (IssueLink) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
