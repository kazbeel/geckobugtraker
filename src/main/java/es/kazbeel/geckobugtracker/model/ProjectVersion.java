package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "PROJECT_VERSIONS")
@NamedQueries({@NamedQuery(name = "ProjectVersion.findByProjectId", query = "FROM ProjectVersion WHERE project = :id"),
               @NamedQuery(name = "ProjectVersion.decreaseSortValue", query = "UPDATE ProjectVersion SET sortValue = CASE WHEN (sortValue > 0) THEN (sortValue - 1) ELSE (sortValue) END WHERE id = :id"),
               @NamedQuery(name = "ProjectVersion.increaseSortValue", query = "UPDATE ProjectVersion SET sortValue = (sortValue + 1) WHERE id = :id")})
public class ProjectVersion implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NAME", length = 255, unique = true, nullable = false)
    private String name;
    
    @Size(max = 4096)
    @Column(name = "DESCRIPTION", length = 4096)
    private String description;
    
    @NotNull
    @Min(value = 0)
    @Column(name = "SORT_VALUE", nullable = false)
    private Integer sortValue = 0;
    
    @NotNull
    @Column(name = "RELEASED", nullable = false)
    private Boolean released = false;
    
    @NotNull
    @Column(name = "ARCHIVED", nullable = false)
    private Boolean archived = false;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RELEASED_DATE")
    private Date releasedDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_ON", nullable = false, updatable = false)
    private Date createdOn;
    
    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name = "PROJECT_ID", nullable = false)
    private Project project;
    
    
    public ProjectVersion () {
    
    }
    
    public Long getId () {
    
        return id;
    }
    
    public void setId (Long id) {
    
        this.id = id;
    }
    
    public String getName () {
    
        return name;
    }
    
    public void setName (String name) {
    
        this.name = name;
    }
    
    public String getDescription () {
    
        return description;
    }
    
    public void setDescription (String description) {
    
        this.description = description;
    }
    
    public Integer getSortValue () {
    
        return sortValue;
    }
    
    public void setSortValue (Integer sortValue) {
    
        this.sortValue = sortValue;
    }
    
    public Boolean getReleased () {
    
        return released;
    }
    
    public void setReleased (Boolean released) {
    
        this.released = released;
        
        if ((released != null) && (released == true)) {
            this.releasedDate = new Date();
        }
    }
    
    public Boolean getArchived () {
    
        return archived;
    }
    
    public void setArchived (Boolean archived) {
    
        this.archived = archived;
    }
    
    public Date getReleasedDate () {
    
        return releasedDate;
    }
    
    public void setReleasedDate (Date releasedDate) {
    
        this.releasedDate = releasedDate;
    }
    
    public Date getCreatedOn () {
    
        return createdOn;
    }
    
    public void setCreatedOn (Date createdOn) {
    
        this.createdOn = createdOn;
    }
    
    public Project getProject () {
    
        return project;
    }
    
    public void setProject (Project project) {
    
        this.project = project;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ProjectVersion))
            return false;
        ProjectVersion other = (ProjectVersion) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
