package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;


public class User implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    private Integer id;
    
    @NotNull
    @Size(min = 1, max = 128)
    private String loginName;
    
    @Size(max = 5)
    private String shortcut;
    
    @NotNull
    @Size(max = 64)
    private String password;
    
    @NotNull
    private boolean enabled = true;
    
    @NotNull
    @Size(min = 1, max = 64)
    private String firstName;
    
    @NotNull
    @Size(min = 1, max = 128)
    private String lastName;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Email
    private String email;
    
    private Date createdOn;
    
    private Date lastUpdateOn;
    
    private Set<Group> groups = new HashSet<Group>(0);
    
    
    public User () {

    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public String getLoginName () {
    
        return loginName;
    }
    
    public void setLoginName (String loginName) {
    
        this.loginName = loginName;
    }
    
    public String getShortcut () {
    
        return shortcut;
    }
    
    public void setShortcut (String shortcut) {
    
        this.shortcut = shortcut;
    }
    
    public String getPassword () {
    
        return password;
    }
    
    public void setPassword (String password) {
    
        this.password = password;
    }
    
    public boolean isEnabled () {
    
        return enabled;
    }
    
    public void setEnabled (boolean enabled) {
    
        this.enabled = enabled;
    }
    
    public String getFirstName () {
    
        return firstName;
    }
    
    public void setFirstName (String firstName) {
    
        this.firstName = firstName;
    }
    
    public String getLastName () {
    
        return lastName;
    }
    
    public void setLastName (String lastName) {
    
        this.lastName = lastName;
    }
    
    public String getEmail () {
    
        return email;
    }
    
    public void setEmail (String email) {
    
        this.email = email;
    }
    
    public Set<Group> getGroups () {
    
        return groups;
    }
    
    public void setGroups (Set<Group> groups) {
    
        this.groups = groups;
    }
    
    public Date getCreatedOn () {
    
        return createdOn;
    }
    
    public void setCreatedOn (Date createdOn) {
    
        this.createdOn = createdOn;
    }
    
    public Date getLastUpdateOn () {
    
        return lastUpdateOn;
    }
    
    public void setLastUpdateOn (Date lastUpdate) {
    
        this.lastUpdateOn = lastUpdate;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((loginName == null) ? 0 : loginName.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (loginName == null) {
            if (other.loginName != null)
                return false;
        } else if (!loginName.equals(other.loginName))
            return false;
        return true;
    }
    
}
