package es.kazbeel.geckobugtracker.service;


import java.util.Collection;

import es.kazbeel.geckobugtracker.model.Attachment;
import es.kazbeel.geckobugtracker.model.Category;
import es.kazbeel.geckobugtracker.model.Comment;
import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.IssueLink;
import es.kazbeel.geckobugtracker.model.IssueLinkType;
import es.kazbeel.geckobugtracker.model.IssueType;
import es.kazbeel.geckobugtracker.model.Priority;
import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.model.Status;
import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;


public interface IssueService {
    
    /* Issues */
    
    public Issue getIssueById (Integer issueId);
    
    public Page<Issue> getIssuesByReporter (User reporter, PageRequest pageRequest);
    
    public Page<Issue> getIssuesVotedByUser (User voter, PageRequest pageRequest);
    
    public Page<Issue> getIssuesWatchedByUser (User watcher, PageRequest pageRequest);
    
    public Page<Issue> getAllIssues (PageRequest pageRequest);
    
    public Page<Issue> getIssuesByAssigneeAndResolution (User assignee, Resolution resolution, PageRequest pageRequest);
    
    public Collection<Issue> getAddedRecentlyIssuesTable ();
    
    public void saveIssue (Issue issue);
    
    public Long getNumIssuesByReporter (User reporter);
    
    public Long getNumIssuesWatchedByUser (User watcher);
    
    public Long getNumIssuesVotedByUser (User voter);
    
    public void deleteIssue (Issue issue);
    
    /* Comments */
    
    public void addCommentToIssue (Issue issue, User user, Comment comment);
    
    /* Votes */
    
    public Long getNumVotesForIssue (Issue issue);
    
    public Boolean isIssueVotedByUser (Issue issue, User voter);
    
    public void voteIssue (User user, Issue issue, boolean vote);
    
    /* Watchers */
    
    public Long getNumWatchersForIssue (Issue issue);
    
    public Boolean isIssueWatchedByUser (Issue issue, User watcher);
    
    public void watchIssue (User user, Issue issue, boolean watch);
    
    /* Category */
    
    public void saveCategory (Category category);
    
    public void deleteCategory (Category category);
    
    public Collection<Category> getAllCategories ();
    
    public Category getCategoryById (Integer id);
    
    /* Issue Types */
    
    public void saveIssueType (IssueType issueType);
    
    public void deleteIssueType (IssueType issueType);
    
    public Collection<IssueType> getAllIssueTypes ();
    
    public IssueType getIssueTypeById (Integer id);
    
    /* Issue Link Types */
    
    public void saveIssueLinkType (IssueLinkType issueLinkType);
    
    public void deleteIssueLinkType (IssueLinkType issueLinkType);
    
    public Collection<IssueLinkType> getAllIssueLinkTypes ();
    
    public IssueLinkType getIssueLinkTypeById (Integer id);
    
    /* Issue Links */
    
    public IssueLink getIssueLinkById (Integer id);
    
    public Collection<IssueLink> getAllIssueLinksWithSource (Issue issue);
    
    public Collection<IssueLink> getAllIssueLinksWithDestination (Issue issue);
    
    public Collection<IssueLink> getAllIssueLinksRelatedToIssue (Issue issue);
    
    public void saveIssueLink (IssueLink issueLink);
    
    public void deleteIssueLink (IssueLink issueLink);
    
    /* Statuses */
    
    public void saveStatus (Status status);
    
    public void deleteStatus (Status status);
    
    public Collection<Status> getAllStatuses ();
    
    public Status getStatusById (Integer id);
    
    /* Priorities */
    
    public void savePriority (Priority priority);
    
    public void deletePriority (Priority priority);
    
    public Collection<Priority> getAllPriorities ();
    
    public Priority getPriorityById (Integer id);
    
    public void setHigherOrderForPriority (Priority priority);
    
    public void setLowerOrderForPriority (Priority priority);
    
    /* Resolutions */
    
    public void saveResolution (Resolution resolution);
    
    public void deleteResolution (Resolution resolution);
    
    public Collection<Resolution> getAllResolutions ();
    
    public Resolution getResolutionById (Integer id);
    
    public void setHigherOrderToResolution (Resolution resolution);
    
    public void setLowerOrderToResolution (Resolution resolution);
    
    /* Environments */
    
    public Collection<Environment> getAllEnvironments ();
    
    public Environment getEnvironmentById (Integer id);
    
    public void saveEnvironment (Environment environment);
    
    public void deleteEnvironment (Environment environment);
    
    /* Tags */
    
    public Collection<Tag> getAllTags ();
    
    public Tag getTagById (Integer id);
    
    public void saveTag (Tag tag);
    
    public void deleteTag (Tag tag);
    
    /* Attachments */
    
    public void saveAttachment (Attachment attachment);
    
    public Attachment getAttachmentById (Integer attachmentId);
    
    public Collection<Attachment> getAllAttachmentsFromIssue (Issue issue);
}
