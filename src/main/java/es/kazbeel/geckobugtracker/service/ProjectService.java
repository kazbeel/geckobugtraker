package es.kazbeel.geckobugtracker.service;


import java.util.Collection;

import es.kazbeel.geckobugtracker.model.Component;
import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.model.ProjectVersion;
import es.kazbeel.geckobugtracker.model.Section;
import es.kazbeel.geckobugtracker.model.User;


public interface ProjectService {
    
    /* Section */
    
    public Section getSectionById (Long id);
    
    public Section getSectionByIdWithProjects (Long id);

    public Collection<Section> getAllSections ();
    
    public Section getSectionByName (String name);
    
    public void setHigherOrderToSection (Section section);
    
    public void setLowerOrderToSection (Section section);
    
    public void saveSection (Section section);
    
    public void deleteSection (Section section);
    
    /* Project */
    
    public Project getProjectById (Long projectId);
    
    public Project getProjectByIdWithSharedComponentsAndVersions (Long projectId);

    public Collection<Project> getAllProjects ();
    
    public Project getProjectByName (String name);
    
    public Collection<Project> getProjectsBySection (Section section);
    
    public Collection<Project> getProjectsByDevLeader (User devLeader);
    
    public Collection<Project> getProjectsByQALeader (User qaLeader);
    
    public void saveProject (Project project);
    
    public void deleteProject (Project project);
    
    public void saveProjectWithChildComponent (Project project);

    /* Project Version */
    
    public ProjectVersion getProjectVersionById (Long projectVersionId);

    public Collection<ProjectVersion> getProjectVersionsByProject (Project project);
    
    public void setHigherOrderToProjectVersion (ProjectVersion projectVersion);
    
    public void setLowerOrderToProjectVersion (ProjectVersion projectVersion);

    public void saveProjectVersion (ProjectVersion projectVersion);
    
    public void deleteProjectVersion (ProjectVersion projectVersion);
    
    /* Component */
    
    public Component getComponentById (Long componentId);
    
    public Component getComponentByName (String name);
    
    public Collection<Component> getComponentsByProject (Project project);
    
    public void saveComponent (Component component);
    
    public void deleteComponent (Component component);
    
    public Component getComponentByIdWithSharedProjects (Long componentId);

    public Collection<Project> getShareableProjectsForComponent (Component component);
    
    public void shareComponentWithProject (Component component, Project project);

    public void unshareCompronentFromProject (Component component, Project project);
}
