package es.kazbeel.geckobugtracker.service;


import java.util.Collection;

import org.springframework.dao.DataAccessException;

import es.kazbeel.geckobugtracker.model.Group;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;


public interface UserGroupService {
    
    /* Users */    
    public Page<User> getAllUsers (PageRequest pageRequest) throws DataAccessException;
    
    public User getUserById (Integer userId) throws DataAccessException;
    
    public Collection<User> getAllEnabledUsers () throws DataAccessException;
    
    public void saveUser (User user) throws DataAccessException;

    public void deleteUser (User user) throws DataAccessException;

    /* Groups */
    
    public Page<Group> getAllGroups (PageRequest pageRequest) throws DataAccessException;

    public Collection<Group> getAllEnabledGroups () throws DataAccessException;

    public Group getGroupById (Integer groupId) throws DataAccessException;

    public void saveGroup (Group group) throws DataAccessException;

    public void deleteGroup (Group group) throws DataAccessException;
}
