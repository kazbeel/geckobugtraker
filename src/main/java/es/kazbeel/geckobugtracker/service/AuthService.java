package es.kazbeel.geckobugtracker.service;


import java.util.Collection;
import java.util.Date;

import org.springframework.dao.DataAccessException;

import es.kazbeel.geckobugtracker.model.LoginInfo;
import es.kazbeel.geckobugtracker.model.RemembermeToken;


public interface AuthService
{
    /* Remember Me Token */
    
    public Collection<RemembermeToken> getRemembermeTokenByUsername (String username) throws DataAccessException;
    
    public RemembermeToken getRemembermeTokenBySeries (String series) throws DataAccessException;
    
    public void saveToken (RemembermeToken remembermeToken) throws DataAccessException;
    
    public void saveToken (String series, String token, Date createdOn) throws DataAccessException;
    
    public void deleteTokensFromUsername (String username) throws DataAccessException;
    
    /* Login Info */
    
    public LoginInfo getLoginInfoByUsername (String username) throws DataAccessException;
    
    public void resetFailAttemptsFromLoginInfo (String username) throws DataAccessException;

    public void increaseFailAttemptsFromLoginInfo (String username) throws DataAccessException;

    public void saveLoginInfo (LoginInfo loginInfo) throws DataAccessException;
    
    public void deleteLoginInfoFromUsername (String username);
}
